#!/bin/bash
#deploy testing merge
rm public/assets/*
git stash
git pull
rake db:drop db:create db:migrate db:seed assets:precompile RAILS_ENV=production
git stash apply
service apache2 restart
