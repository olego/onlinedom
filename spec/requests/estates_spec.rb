require 'rails_helper'

RSpec.describe "Estates", type: :request do
    before(:each) do
        @user = FactoryGirl.create(:user_company)
        @company = @user.company
        post '/login', email: @user.email, password: @user.password
    end


    describe "GET /api/estates" do
        it "should load estates list" do
            get '/api/estates'
            expect(response).to have_http_status(200)
        end
    end

    describe "GET /api/estates/:estate_id" do
        it "should load estate" do
            @estate = FactoryGirl.create(:estate, company: @company)
            get "/api/estates/#{@estate.id}"
            expect(response).to have_http_status(200)
        end

        it "should not return others company estate" do
            other_estate = FactoryGirl.create(:estate, company_id: @company.id + 1)

            get "/api/estates/#{other_estate.id}"
            expect(response).to have_http_status(404)
        end
    end

    describe "POST /api/estates" do
        it 'should create new estate with rooms' do
            estate_attrs = attributes_for(:estate)
            rooms_number = estate_attrs[:meta]['housing_number'] + 
                estate_attrs[:meta]['nonresident_number']

            expect{
                post '/api/estates', estate_attrs
            }.to change{Room.count}.by rooms_number

            expect(response).to have_http_status(201)

            response_estate = JSON.parse(response.body)
            expect(response_estate['rooms'].length).to eq(rooms_number)
        end

        it 'should link existed rooms to estate, if any' do
            # runned creaete to build address
            estate = FactoryGirl.create(:estate).destroy
            rooms_number = estate.meta['housing_number'] + 
                estate.meta['nonresident_number']

            rooms = Room.create([
                { address: estate.full_address, number: 1, room_type: 'Квартира'},
                { address: estate.full_address, number: 2, room_type: 'Квартира'},
                { address: estate.full_address, number: 3, room_type: 'Квартира'}
            ])

            post '/api/estates', attributes_for(:estate, address: estate.address)
            expect(response).to have_http_status(201)
            response_estate = JSON.parse(response.body)

            response_estate_id = response_estate['id'].to_i;

            rooms.each do |room|
                expect(Room.find(room.id).estate_id).to eq(response_estate_id)
            end

            expect(response_estate['rooms'].length).to eq(rooms_number)
        end

        it 'cant be possible to create 2 estates with the same address' do
            @estate = FactoryGirl.create(:estate)
            post '/api/estates', attributes_for(:estate, address: @estate.address)

            expect(response).to have_http_status(422)
        end
    end
    describe 'PATCH/PUT /api/tickets/:id/status' do
        it 'should update estate' do
            @estate = FactoryGirl.create(:estate, company_id: @user.company.id)
            put "/api/estates/#{@estate.id}", {
                    address: {
                        region: 'Gomel',
                        city: 'Petrikov',
                        street: 'Moon',
                        number: '23',
                        building: '8'
                        } }
            expect(response).to have_http_status(200)

            response_estate = JSON.parse(response.body)
            es = Estate.find(@estate.id)
            expect(response_estate['address']['city']).to eq('Petrikov')
        end
        it 'cant update estate with wrong address' do
            @estate = FactoryGirl.create(:estate, company_id: @user.company.id)
            put "/api/estates/#{@estate.id}", {
                    address: 'green street 9' }
            expect(response).to have_http_status(422)
        end
    end
    describe 'DELETE /api/estates/:id' do
        it 'should destroy estate' do
            @estate = FactoryGirl.create(:estate, company_id: @user.company.id)
            delete "/api/estates/#{@estate.id}"

            expect(response).to have_http_status(200)
        end
    end
    describe 'GET /api/estates/search' do
        it 'should search estates' do
            query = 'dolor'
            FactoryGirl.create_list(:estate, 15, company_id: @user.company.id)
            get "/api/estates/search/#{query}"

            expect(response).to have_http_status(200)

            response_estate = JSON.parse(response.body)
            expect(response_estate.length).to eq(10)
        end
    end
     describe 'GET /api/estates/:id/download' do
        it 'should respond with file' do
            @estate = FactoryGirl.create(:estate, company_id: @user.company.id)
            get "/api/estates/#{@estate.id}/download.xls"
            expect(response).to have_http_status(200)
            # expect(response.body).to include @estate.address['city']
        end
        # it 'should respond file with room counters' do
        #     @estate = FactoryGirl.create(:estate, company_id: @user.company.id)
        #     @room = create(:room, user: @user, counters_count: 3, estate_id: @estate.id)
        #     create(:report, room_id: @room.id)
        #     get "/api/estates/#{@estate.id}/download.xls"
        #     expect(response).to have_http_status(200)
        #     expect(response.body).to include @estate.address['city']
        # end
    end
end
