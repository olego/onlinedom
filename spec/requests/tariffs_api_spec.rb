require 'rails_helper'

RSpec.describe "Tariffs", type: :request do
  before(:each) do
    @user = FactoryGirl.create(:user_company)
    @company = @user.company
    post '/login', email: @user.email, password: @user.password
  end

  describe "GET /api/tariffs/list" do
    it "should load tariffs list" do
      get '/api/tariffs/list'
      expect(response).to have_http_status(200)
    end
  end

  describe "GET /api/tariffs/archive/:tariff_id" do
    it "should load archive tariffs" do
      @estate = FactoryGirl.create(:estate, company_id: @company.id)
      @tariff = FactoryGirl.create(:tariff, estate_id: @estate.id)
      @archive_tariffs = FactoryGirl.create(:archive_tariff, tariff_id: @tariff.id)

      get "/api/tariffs/archive/#{@tariff.id}"
      expect(response).to have_http_status(200)
    end
  end

  describe "GET /api/tariffs/:tariff_id" do
    it "should load tariff" do
      @tariff = FactoryGirl.create(:tariff)
      get "/api/tariffs/#{@tariff.id}"
      expect(response).to have_http_status(200)
    end
  end

  describe "POST /api/tariffs" do
    it 'should create new tariffs for estate' do
      tariff_attrs = FactoryGirl.attributes_for(:tariff)
      tariff_attrs[:value] = 120

      expect{
          post '/api/tariffs', tariff_attrs
      }.to change{Tariff.count}.by 1

      expect(response).to have_http_status(201)
    end

    it 'cant be possible to create 2 tariffs with the same name for same estates' do
      @estate = FactoryGirl.create(:estate)
      @tariff = FactoryGirl.create(:tariff, estate_id: @estate.id)

      post '/api/tariffs', attributes_for(:tariff, estate_id: @estate.id)

      expect(response).to have_http_status(422)
    end
  end

  describe 'DELETE api/tariffs/:tariff_id' do
    it 'should destroy old tariff' do
      @estate = FactoryGirl.create(:estate, company_id: @company.id)
      @tariff = FactoryGirl.create(:tariff, estate_id: @estate.id)

      expect {
        delete "/api/tariffs/#{@tariff.id}"
      }.to change{ Tariff.count }.by -1

      expect(response).to have_http_status(200)
    end
  end
  
  describe 'PATCH/PUT api/tariffs/:tariff_id' do
    it 'should update tariff' do 
      @estate = FactoryGirl.create(:estate, company_id: @company.id)
      @tariff = FactoryGirl.create(:tariff, estate_id: @estate.id)

      put "/api/tariffs/#{@tariff.id}", { value: 199.9 }

      expect(response).to have_http_status(200)

      response_tariff = JSON.parse(response.body)
      tariff = Tariff.find(@tariff.id)

      expect(response_tariff["value"]).to eq(tariff.value)
    end
    
    it 'should create new ArchiveTariff if user will change value' do
      @estate = FactoryGirl.create(:estate, company_id: @company.id)
      @tariff = FactoryGirl.create(:tariff, estate_id: @estate.id, value: 100)

      expect{
        put "/api/tariffs/#{@tariff.id}", { value: 255 }
      }.to change{ArchiveTariff.count}.by 1

      expect(ArchiveTariff.find_by(tariff_id: @tariff.id).new_value).to eql 255
    end
  end
end
