require 'rails_helper'

RSpec.describe "Estates", type: :request do
    before(:each) do
        @user = create(:user_client)
        post '/login', email: @user.email, password: @user.password

        @room = create(:room, user: @user, counters_count: 3)

        @reports = []
        @report_fields = ['id', 'room_id', 'room_address', 'owner_id', 'owner_name', 'report_entries_attributes']
        5.times { @reports << create(:report, room_id: @room.id) }
    end

    describe "GET /api/reports/" do
        before(:each) do
            get "/api/reports"
            expect(response).to have_http_status(200)
            @reports = response_body
        end

        it "should return user's reports list" do
            expect(@reports.count).to eq(@user.reports.count)
        end

        it "should contain proper fields for each record" do
            @reports.each do |report|
                @report_fields.each { |f| expect(report[f]).to be_present }
            end
        end

        it "should return company's reports" do
            @company = create(:company)
            
            post '/login', 
                email: @company.user.email, 
                password: @company.user.password

            get "/api/reports"
            expect(response).to have_http_status(200)

            @reports = response_body
            expect(@reports.count).to eq(@company.reports.count)
        end
    end

    describe "GET /api/reports/:id" do
        before(:each) do
            @report = @reports.sample
        end

        it 'should return report info' do
            get "/api/reports/#{@report.id}"
            expect(response).to have_http_status(200)
        end

        it 'should return report only if it belongs to user' do
            other_report = create(:report, room: create(:room))
            get "/api/reports/#{other_report.id}"
            expect(response).to have_http_status(404)
        end

        it 'should have proper fields' do
            get "/api/reports/#{@report.id}"
            report = response_body
            @report_fields.each { |f| expect(report[f]).to be_present }
        end
    end

    describe "POST /api/reports" do
        it 'should create new report for user' do
            @estate = create(:estate)
            @room = create(:room, user: @user, counters_count: 0, estate_id: @estate.id)
            expect {
                post '/api/reports', attributes_for(:report, room_id: @room.id) 
            }.to change { @user.reports.count }.by(1)
        end

        it 'should not create report if there is a report in a last month' do
            expect {
                post '/api/reports', attributes_for(:report, room_id: @room.id) 
            }.not_to change { @user.reports.count }
        end
    end

    describe "PATCH /api/reports/id" do
        before(:each) do
            @report = @reports.sample
        end

        it "description" do
            attributes = @report.attributes
            attributes['report_entries_attributes'] = 
                @report.report_entries.map{ |e|
                    e.value = rand * 10
                    e.attributes
                }

            patch "/api/reports/#{@report.id}"
            @report.report_entries.each.with_index do |e, i|
                expect(e.value).to eq(attributes['report_entries_attributes'][i]['value'])
            end
        end
    end

    describe "DELETE /api/reports/:id" do
        before(:each) do
            @report = @reports.sample
        end

        it 'should delete report' do
            expect {
                delete "/api/reports/#{@report.id}"
            }.to change{ @user.reports.count }.by(-1)
        end

        it 'should delete associated report_entries' do
            report_entries_count = @report.report_entries.count
            expect {
                delete "/api/reports/#{@report.id}"
            }.to change { ReportEntry.count }.by(-report_entries_count)
        end
    end

    def response_body
        JSON.parse(response.body)
    end
end
