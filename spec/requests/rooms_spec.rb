require 'rails_helper'

RSpec.describe "Rooms", type: :request do
    before(:each) do
        # @user = User.find_by(email: "djently@yandex.ru")
        @user = create(:user_client)
        post '/login', email: @user.email, password: @user.password

        @room = Room.create(
            user: @user,
            number: 45,
            address: 'test address',
            room_type: 'Квартира',
            company_name: 'test company',
            company_address: 'test company_address',
            company_phone: '343564',
            meta: {test: 'test'},
            counters_attributes: [{
                    place: 'Кухня',
                    counter_type: 'Отопление',
                    regnum: '12344525',
                    name: 'test counter',
                    precision: '0.5',
                    seal_date: '24-05-2015',
                    seal_number: '24525244',
                    replacement_date: '25-06-2018'
                }]
        )
    end

    describe "GET /api/rooms" do
        it "should load rooms list" do
            get '/api/rooms'
            expect(response).to have_http_status(200)
        end 
    end

    describe "GET /api/rooms/:id" do
        context 'if client' do
            it 'should render room info if user owns a room' do
                get "/api/rooms/#{@room.id}"
                expect(response).to have_http_status(200)
            end
            it 'cant get room info if user not owns a room' do
                @user = create(:user_client)
                post '/login', email: @user.email, password: @user.password

                get "/api/rooms/#{@room.id}"
                expect(response).to have_http_status(404)
            end
        end
        context 'if company' do
            before(:each) do
                @user = create(:user_company)
                post '/login', email: @user.email, password: @user.password
                @estate = create(:estate, company_id: @user.company.id)
                @room = create(:room, user: @user, counters_count: 3, estate_id: @estate.id)
            end
            it 'should render room info' do
                get "/api/rooms/#{@room.id}"
                expect(response).to have_http_status(200)
            end
            it 'cant get not own room' do
                @user = create(:user_company)
                post '/login', email: @user.email, password: @user.password

                get "/api/rooms/#{@room.id}"
                expect(response).to have_http_status(404)
            end
        end
    end

    describe "POST /api/rooms" do
        it 'should create new room with nested counter' do
            expect{
                post '/api/rooms',
                    address: 'test address',
                    room_type: 'Квартира',
                    number: 44,
                    company_name: 'test company',
                    company_address: 'test company_address',
                    company_phone: '343564',
                    meta: {test: 'test'},
                    counters_attributes: [{
                            place: 'Кухня',
                            counter_type: 'Отопление',
                            regnum: '12344525',
                            name: 'test counter',
                            precision: '0.5',
                            seal_date: '24-05-2015',
                            seal_number: '24525244',
                            replacement_date: '25-06-2018'
                        }]
            }.to change{@user.rooms.count}.by(1)

            expect(@user.rooms.last.counters).to be_present
        end

        it 'should bind room to existing estate' do
            @estate = create(:estate)
            post '/api/rooms', attributes_for(
                :room, address: @estate.full_address, number: 1
            )

            response_room = JSON.parse(response.body)
            expect(response_room['estate_id']).to eq(@estate.id)
        end

        it 'should replace room for existing estate' do
            @estate = create(:estate)

            expect{
                post '/api/rooms', attributes_for(
                    :room, address: @estate.full_address, number: 1
            )}.not_to change{ @estate.rooms.count }
        end

        it 'cant be possible to change rooms owner' do
            @estate = create(:estate)
            @user1 = create(:user_client)

            @estate.rooms.first.update(user_id: @user1.id)

            post '/api/rooms', attributes_for(
                :room, address: @estate.full_address, number: 1
            )

            expect(response).to have_http_status(422)
        end
    end

    describe "PUT " do
        it 'should update model' do
            patch "/api/rooms/#{@room.id}",
                address: 'test address',
                room_type: 'Квартира',
                number: 43,
                counters_attributes: [{
                        place: 'Кухня',
                        counter_type: 'Отопление',
                        replacement_date: '25-06-2018'
                    }]
            expect(response).to have_http_status(200)
            expect(@user.rooms.last.counters.count).to be(2)
        end

        it 'should remove counter on update' do
            expect{
                patch "/api/rooms/#{@room.id}",
                    counters_attributes: [{
                            _destroy: true,
                            id: @room.counters.last.id
                        }]
            }.to change{@room.counters.count}.by(-1)
        end

        it 'cant update with wrong params' do
            patch "/api/rooms/#{@room.id}",
                address: ''
            expect(response).to have_http_status(422)
        end

        it 'should set owner data if owner not registered' do
            @user = create(:user_company)
            post '/login', email: @user.email, password: @user.password
            @estate = create(:estate, company_id: @user.company.id)
            @room = create(:room, estate_id: @estate.id)
            patch "/api/rooms/#{@room.id}",
                owner: {
                    'name' => 'testname',
                    'email' => '452352346565363@mail.ru'
                }
            expect(response).to have_http_status(200)

            response_room = JSON.parse(response.body)
            expect(response_room['owner']['registered']).to be(false)
        end

        it 'should bind room to existing user when pass owner' do
            patch "/api/rooms/#{@room.id}",
                owner: {
                    'name' => 'testname',
                    'email' => @user.email
                }
            expect(response).to have_http_status(200)

            response_room = JSON.parse(response.body)
            expect(response_room['owner']['registered']).to be(true)
        end
    end

    describe "DELETE /api/rooms/id" do
        it 'should update room as deleted' do
            delete "/api/rooms/#{@room.id}"
            expect(response).to have_http_status(200)
            room = Room.find_by(id: @room.id)
            expect(room).not_to be_nil
        end

        it 'shouldn\'t destroy nested room counters' do
            expect{
                delete "/api/rooms/#{@room.id}"
            }.not_to change{Counter.count}
            expect(response).to have_http_status(200)
        end
    end
end
