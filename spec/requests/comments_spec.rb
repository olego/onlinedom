require 'rails_helper'

RSpec.describe "Comments", type: :request do
    before(:each) do
        @user = create(:user_company)
        @company = @user.company
        post '/login', email: @user.email, password: @user.password

        @estate = create(:estate)
        @room = create(:room, user: @user, counters_count: 3, estate_id: @estate.id)
        @executor = create(:user_client)
    end

    describe "GET /api/comments" do
        let(:ticket){create(:ticket, user_id: @user.id, room_id: @room.id, executor_id: @executor.id)}
        it "should create comment" do
            params = build(:comment, user_id: @user.id, commentable_type: 'Ticket', commentable_id: ticket.id).attributes
            post '/api/comments', params
            expect(response).to have_http_status(201)
        end
        it "cant create empty comment" do
            params = build(:comment, user_id: @user.id, commentable_type: 'Ticket', commentable_id: ticket.id, body: '').attributes
            post '/api/comments', params
            expect(response).to have_http_status(422)
        end
        it "should unwatch subscriptions expect commentator" do
            executor_subscription = Subscription.find_by(user_id: @executor.id, ticket_id: ticket.id)
            executor_subscription.read!
            user_subscription = Subscription.find_by(user_id: @user.id, ticket_id: ticket.id)
            user_subscription.read!
            params = build(:comment, user_id: @user.id, commentable_type: 'Ticket', commentable_id: ticket.id).attributes
            post '/api/comments', params
            executor_subscription.reload
            expect(executor_subscription.watched).to be false
            user_subscription.reload
            expect(user_subscription.watched).to be true
        end
    end
end
