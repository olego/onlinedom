require 'rails_helper'

RSpec.describe "Tickets", type: :request do
    before(:each) do
        @user = create(:user_client)
        post '/login', email: @user.email, password: @user.password

        @estate = create(:estate)
        @room = create(:room, user: @user, counters_count: 3, estate_id: @estate.id)
    end

    describe "GET /api/tickets" do
        it "should get list of tickets" do
            get '/api/tickets'
            expect(response).to have_http_status(200)
        end

        it "should get list of tickets for company" do
            @user = create(:user_company)
            post '/login', email: @user.email, password: @user.password

            get '/api/tickets'
            expect(response).to have_http_status(200)
        end
    end

    describe 'GET /api/tickets/:id' do
        it 'should respond with ticket model' do
            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id)
            get "/api/tickets/#{@ticket.id}"
            expect(response).to have_http_status(200)
        end

        it 'should change ticket status when executor watches ticket' do
            @user = create(:user_company)
            post '/login', email: @user.email, password: @user.password

            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id, executor_id: @user.id)

            get "/api/tickets/#{@ticket.id}"
            expect(response).to have_http_status(200)

            @ticket = Ticket.find(@ticket.id)
            expect(@ticket.status).to eq('Просмотрена')
        end

        it "cant get any random ticket" do
            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id)
            @user1 = create(:user_company)
            post '/login', email: @user1.email, password: @user1.password

            get "/api/tickets/#{@ticket.id}"
            expect(response).to have_http_status(404)
        end
    end

    describe 'POST /api/tickets' do
        it 'should be possible to create ticket for client' do
            post '/api/tickets', attributes_for(:ticket, room_id: @room.id)
            expect(response).to have_http_status(201)
        end

        it 'should be possible to create ticket for company' do
            @user = create(:user_company)
            post '/login', email: @user.email, password: @user.password
            @estate = create(:estate, company_id: @user.company.id)
            @room = create(:room, user: @user, counters_count: 3, estate_id: @estate.id)
            post '/api/tickets', attributes_for(:ticket_for_company, room_id: @room.id, executor_id: @user.id)
            expect(response).to have_http_status(201)
        end

        it 'cant be possible to create ticket for client with room number' do
            post '/api/tickets', attributes_for(:ticket, room_number: @room.number, estate_id: @estate.id)
            expect(response).to have_http_status(422)
        end

        it 'should be possible to create ticket for company with room number' do
            @user = create(:user_company)
            post '/login', email: @user.email, password: @user.password

            @estate = create(:estate, company_id: @user.company.id)
            @room = create(:room, user: @user, counters_count: 3, estate_id: @estate.id)
            post '/api/tickets', attributes_for(:ticket, room_number: @room.number, estate_id: @estate.id)
            expect(response).to have_http_status(201)
        end

        it 'should create ticket with statuses' do
            post '/api/tickets', attributes_for(
                :ticket, room_id: @room.id)
            expect(response).to have_http_status(201)
            expect(Ticket.last.ticket_type).to eq('Обычная')

            post '/api/tickets', attributes_for(
                :ticket, room_id: @room.id, ticket_type: 'Экстренная')
            expect(response).to have_http_status(201)
            expect(Ticket.last.ticket_type).to eq('Экстренная')

            post '/api/tickets', attributes_for(
                :ticket, room_id: @room.id, ticket_type: 'Платная')
            expect(response).to have_http_status(201)
            expect(Ticket.last.ticket_type).to eq('Платная')
        end
    end

    describe 'PATCH/PUT /api/tickets/:id' do
        it 'should update ticket' do
            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id)
            put "/api/tickets/#{@ticket.id}", attributes_for(
                :ticket, ticket_type: 'Экстренная')
            expect(response).to have_http_status(200)

            @ticket = Ticket.find(@ticket.id)
            expect(@ticket.ticket_type).to eq('Экстренная')
        end

        it 'cant update ticket with wrong status' do
            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id)

            put "/api/tickets/#{@ticket.id}", {
                status: 'Сделал' }
            expect(response).to have_http_status(422)
        end

        it 'should set finish timer' do
            @user = create(:user_company)
            post '/login', email: @user.email, password: @user.password

            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id, executor_id: @user.id)
            put "/api/tickets/#{@ticket.id}", {
                status: 'Исполнено' }

            @ticket = Ticket.find(@ticket.id)
            expect(@ticket.finished_at).not_to be_nil
        end

        it 'should clear finish timer' do
            @user = create(:user_company)
            post '/login', email: @user.email, password: @user.password

            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id, executor_id: @user.id)
            put "/api/tickets/#{@ticket.id}", {
                status: 'Исполнено' }

            put "/api/tickets/#{@ticket.id}", {
                status: 'В работе' }
            expect(response).to have_http_status(200)

            @ticket = Ticket.find(@ticket.id)
            expect(@ticket.finished_at).to be_nil
        end
    end

    describe 'PATCH/PUT /api/tickets/:id/status' do
        before(:each) do
            @user_company = create(:user_company)
            post '/login', email: @user_company.email, password: @user_company.password

            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id, executor_id: @user_company.id)

            put "/api/tickets/#{@ticket.id}/status", {
                status: 'Исполнено' }
        end
        context 'for new ticket' do
            it 'should update ticket status' do
                expect(response).to have_http_status(200)

                @ticket = Ticket.find(@ticket.id)
                expect(@ticket.status).to eq('Исполнено')
            end
        end
        context 'if client' do
            before(:each) do
                post '/login', email: @user.email, password: @user.password
            end
            it 'should update ticket status' do

                put "/api/tickets/#{@ticket.id}/status", {
                    status: 'Закрыта' }
                expect(response).to have_http_status(200)

                @ticket = Ticket.find(@ticket.id)
                expect(@ticket.status).to eq('Закрыта')
            end
            it 'cant update ticket with wrong status' do
                put "/api/tickets/#{@ticket.id}/status", {
                    status: 'Сделал' }
                expect(response).to have_http_status(422)
            end
            it 'cant update ticket with wrong status' do
                put "/api/tickets/#{@ticket.id}/status"
                expect(response).to have_http_status(422)
            end
        end
        context 'if company' do
            it 'should update ticket status' do
                put "/api/tickets/#{@ticket.id}/status", {
                    status: 'Закрыта' }
                expect(response).to have_http_status(403)
            end
        end
    end

    describe 'DELETE /api/tickets/:id' do
        it 'should ticket status to cancelled' do
            @ticket = create(:ticket, user_id: @user.id, room_id: @room.id)
            
            delete "/api/tickets/#{@ticket.id}"
            expect(response).to have_http_status(200)
            
            @ticket = Ticket.find(@ticket.id)
            expect(@ticket.status).to eq('Отменена')
        end
        it 'cant delete not new ticket if company' do
            @user_company = create(:user_company)
            post '/login', email: @user_company.email, password: @user_company.password
            @ticket = create(:ticket, user_id: @user_company.id, room_id: @room.id)
            @ticket.update(status: 'В работе')
            
            delete "/api/tickets/#{@ticket.id}"
            expect(response).to have_http_status(403)
        end
    end
end
