FactoryGirl.define do
  factory :support do
    name "MyString"
    phones "MyText"
    address "MyString"
    estate nil
  end
end
