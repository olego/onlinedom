FactoryGirl.define do
  factory :tariff do
    name "Содержание общего имущества"
    value "9.99"
    association :estate, factory: :estate
  end
end

