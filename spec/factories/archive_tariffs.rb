FactoryGirl.define do
  factory :archive_tariff do
    association :tariff, factory: :tariff
    old_value "9.99"
    new_value "10.44"
  end
end
