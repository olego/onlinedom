FactoryGirl.define do
  factory :role do
    name "test role"
    permissions({ "employees" => {
          "show" => "true",
          "list" => "true"
        }
      })
    factory :dispatcher do
      name 'dispatcher'
      permissions({ "requests" => {
          "update" => "true",
          "list" => "true"
        }
      })
    end
  end
end
