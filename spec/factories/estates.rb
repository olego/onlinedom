FactoryGirl.define do
  factory :estate do
    sequence :address do |n| {
        'region' => 'lorem',
        'city' => 'ipsum',
        'street' => 'dolor',
        'number' => "21#{n}",
        'building' => "3#{n}"
      }
    end
    license({
      'series' => '666',
      'number' => '543272',
      'issue_date' => '21.02.2012',
      'issued_by' => 'Дядя Вася'
    })
    meta({
      'housing_number' => 10,
      'nonresident_number' => 10
    })
  end
end
