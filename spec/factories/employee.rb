FactoryGirl.define do
    factory :employee do
        association :user, factory: :confirmed_user
        association :company, factory: :company
        association :role, factory: :role
    end
end