FactoryGirl.define do
  factory :report do
    after(:create) do |report, evaluator|
      evaluator.room.counters.each do |counter|
        create(:report_entry, report: report, counter: counter)
      end
    end
  end
end
