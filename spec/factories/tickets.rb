FactoryGirl.define do
  factory :ticket do
    ticket_type 'Обычная'
    meta({
      :description => 'Все сломалось'
    })
    factory :ticket_for_company do
      applicant 'SSw'
    end
  end
end
