require 'rails_helper'

RSpec.describe EmployeesController, type: :controller do
  before(:each) do
    @user = create(:user_company)
    @employer = create(:employee, company_id: @user.company.id)
    @company = @user.company
    session[:user] = @user.id
    session[:user_type] = @user.user_type
  end

  it 'should be forbidden to work with employees for non-company users' do
    user = create(:confirmed_user)
    session[:user] = user.id
    session[:user_type] = user.user_type

    get :list
    expect(response).to have_http_status(403)
  end

  it 'should show employee' do
    request.env["HTTP_ACCEPT"] = 'application/json'
    get :show, id: @employer.id

    expect(response).to have_http_status(200)
  end

  describe 'create employee' do
    it 'should create employee model for existing user' do
      user = create(:confirmed_user, name: 'Vasili')
      post :create, { email: user.email }
      expect(response).to have_http_status(200)
    end

    it 'should create new user if it not exist when creating employee' do
      post :create, { email: 'some1@user.ru', name: 'Vasili Pupkin' }
      expect(response).to  have_http_status(200)

      expect {
        post :create, { email: 'some2@user.ru', name: 'Vasili Pupkin' }
      }.to change{User.count}.by(1)
    end

    it 'should return 422 if selected user already is employee' do
      user = create(:confirmed_user)
      post :create, { email: user.email, name: 'Vasili Pupkin' }
      post :create, { email: user.email, name: 'Vasili Pupkin' }
      expect(response).to have_http_status(422)
    end
  end

  describe 'employee delete' do
    it 'should delete employee' do
      user = create(:confirmed_user)
      post :create, { email: user.email }

      employee = Employee.last
    end

    it 'should return 404 if current company
      doesnt have employee with provided id' do

    end
  end
end
