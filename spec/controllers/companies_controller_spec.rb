require 'rails_helper'

RSpec.describe CompaniesController, type: :controller do
    before(:each) do
        # login as company
        @user = FactoryGirl.create(:user_company)
        session[:user] = @user.id
    end

    it 'should upate unverified company' do
        company_attributes = attributes_for(:company)
        post :update, company_attributes

        expect(response.body).to eq( {success: true}.to_json )
    end

    it 'should create update request for verified company' do
        @user.meta.verify!
        company_attributes = attributes_for(:company)
        post :update, company_attributes

        expect(response.body).to eq({
          success: 'Создан запрос на обновление данных'
          }.to_json )
    end

    it 'can\'t self-update non-company meta' do
      user = create(:user_client)
      session[:user] = user.id

      company_attributes = attributes_for(:company)
      post :update, company_attributes

      expect(response.body).to eq({
        error: 'Пользователь не прикреплен к компании'
        }.to_json)
    end


    it 'should save phone numbers' do
        company_attributes = attributes_for(:company)
        post :update, company_attributes

        expect(@user.meta.phones).to eq(company_attributes[:phones])
    end

    it 'should save coordinates' do
        company_attributes = attributes_for(:company)
        post :update, company_attributes

        expect(@user.meta.coordinates).to eq(company_attributes[:coordinates])
    end

    describe 'files' do
      before(:each) do
        @user = FactoryGirl.create(:user_company)
        session[:user] = @user.id
        @company = create(:company)
        @user.meta = @company
        @logo_file = fixture_file_upload('files/testlogo.png', 'image/png')
      end

      it 'should create logo file handler' do
        expect{
          post :upload_file, {file: @logo_file, type: 'logo'}
        }.to change{
          FileHandler.count
        }.by(1)
      end

      it 'should replace logo file' do
        post :upload_file, {file: @logo_file, type: 'logo'}
        
        expect{
          post :upload_file, {file: @logo_file, type: 'logo'}
        }.not_to change{
          FileHandler.count
        }
      end
      it 'should create innkpp file' do
        expect{
          post :upload_file, {file: @logo_file, type: 'innkpp'}
        }.to change{
          FileHandler.count
        }.by(1)
      end
      it 'should replace innkpp file' do
        post :upload_file, {file: @logo_file, type: 'innkpp'}
        
        expect{
          post :upload_file, {file: @logo_file, type: 'innkpp'}
        }.not_to change{
          FileHandler.count
        }
      end
      it 'should create ogrn file' do
        expect{
          post :upload_file, {file: @logo_file, type: 'ogrn'}
        }.to change{
          FileHandler.count
        }.by(1)
      end
      it 'should replace ogrn file' do
        post :upload_file, {file: @logo_file, type: 'ogrn'}
        
        expect{
          post :upload_file, {file: @logo_file, type: 'ogrn'}
        }.not_to change{
          FileHandler.count
        }
      end
      it 'cant create file with wrong type' do
        post :upload_file, {file: @logo_file, type: 'casual'}
        
        expect(response).to have_http_status(422)
      end
      it 'cant upload logo file without file' do
        post :upload_file, { type: 'logo'}
        expect(response).to have_http_status(404)
      end

      it 'should delete logo_file' do
        post :upload_file, {file: @logo_file, type: 'logo'}

        expect{
          delete :delete_file, type: 'logo'
          }.to change{
            FileHandler.count
          }.by(-1)
      end
      it 'cant delete empty logo_file' do
        delete :delete_file, type: 'logo'
        expect(response).to have_http_status(404)
      end
      it 'cant delete file with wrong type' do
        post :delete_file, { type: 'casual'}
        
        expect(response).to have_http_status(422)
      end
      it 'should get company innkpp file' do
        post :upload_file, {file: @logo_file, type: 'innkpp'}

        get :innkpp_file
        expect(response.headers['Content-Type']).to include 'image/png'
      end
      it 'cant get empty innkpp file' do
        get :innkpp_file
        expect(response).to have_http_status(404)
      end
      it 'should get company ogrn file' do
        post :upload_file, {file: @logo_file, type: 'ogrn'}

        get :ogrn_file
        expect(response.headers['Content-Type']).to include 'image/png'
      end
      it 'cant get empty ogrn file' do
        get :ogrn_file
        expect(response).to have_http_status(404)
      end
    end


    describe 'permissions' do
      it 'can be possible for admin to update company'
      it 'shouldn\'t update other companie\'s model' do
        session[:user] = nil
        company_attributes = attributes_for(:company)
        company_attributes[:id] = @user.id
        post :update, company_attributes

        expect(response).to have_http_status(:redirect)
      end
    end
end
