require 'rails_helper'

RSpec.describe ArchiveTariff, type: :model do
  before { @archive_tariff = FactoryGirl.create(:archive_tariff) }
  subject { @archive_tariff }

  it { should respond_to(:tariff_id) }
  it { should respond_to(:old_value) }
  it { should respond_to(:new_value) }
  it { should respond_to(:created_at) }

  it { should be_valid }
end
