require 'rails_helper'

RSpec.describe Tariff, type: :model do
  before { @tariff = FactoryGirl.create(:tariff) }
  subject { @tariff }

  it { should respond_to(:name) }
  it { should respond_to(:value) }
  it { should respond_to(:estate_id) }

  it { should be_valid }
end
