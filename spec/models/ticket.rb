require 'rails_helper'

RSpec.describe 'Ticket', type: :model do
  let(:user){FactoryGirl.create(:user_company)}
  let(:executor){FactoryGirl.create(:user_client)}
  let(:estate){FactoryGirl.create(:estate, company_id: user.company.id)}
  let(:room){FactoryGirl.create(:room, user: user, counters_count: 3, estate_id: estate.id)}
  let(:ticket){FactoryGirl.create(:ticket, user_id: user.id, room_id: room.id, executor_id: executor.id)}
  it "should set subscribers" do
    expect(ticket.subscribers.count).to eq(2)
  end
  it "should get subscription for user" do
    expect(ticket.get_subscriptions(user)).to_not be_nil
  end
  context 'for employee' do
    let(:role){FactoryGirl.create(:dispatcher, company_id: user.company.id)}
    let(:employee){FactoryGirl.create(:employee)}
    before(:each) do 
      role.employees << employee
    end
    it "should set subscribers by role" do
      ticket.set_subscribers
      expect(ticket.subscribers.count).to eq(3)
      expect(ticket.subscribers).to include employee.user
    end
    it "should create subscription if new" do
      ticket = FactoryGirl.create(:ticket, user_id: user.id, room_id: room.id, executor_id: executor.id)
      role = FactoryGirl.create(:dispatcher, company_id: user.company.id, name: 'Работяга')
      worker = FactoryGirl.create(:employee, role_id: role.id)
      expect{ticket.get_subscriptions(worker.user)}.to change{Subscription.count}.by(1)
    end
  end
end
