class SupportsController < ApplicationController
  # GET /supports
  # GET /supports.json
  def index
    get_company
    @supports = @estate.supports.all
  end

  # GET /supports/1
  # GET /supports/1.json
  def show
    get_company
  end

  # POST /supports
  # POST /supports.json
  def create
    get_company
    @support = Support.new(support_params)
    @support.estate = @estate

    respond_to do |format|
      if @support.save
        format.json { render :show, status: :created}
      else
        format.json { render json: @support.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /supports/1
  # PATCH/PUT /supports/1.json
  def update
    get_company
    respond_to do |format|
      if @support.update(support_params)
        format.json { render :show, status: :ok}
      else
        format.json { render json: @support.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /supports/1
  # DELETE /supports/1.json
  def destroy
    get_company
    @support.destroy
    respond_to do |format|
      format.json { render json: {success: true} }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_support
      @support = @estate.supports.find_by(id: params[:id])
      render json: {error: "Support not found"}, status: 404 unless @support
    end

    def set_estate
      @estate = @company.estates.find_by(id: params[:estate_id])
      render json: {error: "Estate not found"}, status: 404 unless @estate
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def support_params
      params.permit(:name, :address, phones: [:number, :extension])
    end

    def get_company
      unless @company = @user.company
        return render json: {
          error: 'Пользователь не прикреплен к компании'
        }, status: 403
      end

      set_estate if params[:estate_id]
      set_support if params[:id]
    end
end
