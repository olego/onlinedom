class UsersController < ApplicationController
  layout false

  def me
    if session[:user] && @user
      render 'me'
    else
      render nothing: true, status: 404
    end
  end

  def index
    case session[:user_type]
    when 'client'
      return render 'application/client'
    when 'company'
      return render 'application/company'
    when 'admin'
      return render 'application/admin'
    else return render text: 'nothing to do here'
    end
  end

  def login
    if session[:user]
      return redirect_to root_path
    end
    render 'login'
  end

  def auth
    @user = User.find_by(email: params[:email])

    if @user.nil?
      render json: {
        error: 'Нет пользователя с такой почтой/паролем'
      }, status: 404
      return
    end

    if @user.authenticate(params[:password])
      unless @user.confirmed
        return render json: {
            error: 'Пользователь не прошел подтверждение'
          }, status: 403
      end

      # Успешная авторизация
      session[:user] = @user.id
      session[:user_type] = @user.user_type

      if @user.is_employee?
        session[:user_type] = 'company'
      end


      render json: @user.id, status: 200
      return
    else
      render json: {
        error: 'Нет пользователя с такой почтой/паролем'
      }, status: 404
      return
    end
  end

  def set_type_company
    if @user.user_type != 'company' && !@user.is_employee?
      return render json: { error: "Нет доступа к этому действию" }, status: 403
    end

    session[:user_type] = 'company'
    return render nothing: true, status: 200
  end

  def set_type_client
    if @user.user_type != 'company' && !@user.is_employee?
      return render json: { error: "Нет доступа к этому действию" }, status: 403
    end

    session[:user_type] = 'client'
    return render nothing: true, status: 200
  end

  def logout
    session[:user] = nil
    session[:user_type] = nil
    redirect_to root_path
  end

  def create
    user = User.new user_params
    case params[:user_type]
    when 'client'
      user.user_type = 'client'
    when 'company'
      user.user_type = 'company'
      user.company_name = params[:company_name]
    else end

    user.generate_key
      
    if user.save
      render json: user.id, status: 200
      UserMailer.confirm_email(user).deliver_now
    else
      render json: user.errors, status: 422
    end
  end

  def confirm
    unless params[:key]
      @badkey = true
      return render 'confirm', status: 404
    end

    @user = User.find_by(confirmation_key: params[:key])
    unless @user
      @badkey = true
      return render 'confirm', status: 404
    end

    if @user.confirmed
      flash[:warning] = 'Аккаунт уже подтвержден'
      return redirect_to root_path
    end

    if @user.confirm!
      flash[:success] = 'E-mail успешно подтвержден.
        Теперь вы можете авторизоваться.'
      return redirect_to '/login'
    else
      flash[:danger] = 'Ошибка подтверждения e-mail.'
      return redirect_to '/login'
    end
  end

  def update
    @user.assign_attributes profile_params
    if @user.save(validate: false)
      render json: {success: true}
    else
      render json: @user.errors, status: 422
    end
  end

  def upload_userpic
    unless params[:file]
      render json: {error: 'Файл не был отправлен'}, status: 422
    end

    if @user.userpic = params[:file]
      render json: {success: true}
    else
      render json: @user.userpic_errors, status: 422
    end
  end

  def delete_userpic
    if @user.userpic
      if @user.userpic.destroy
        return render json: {success: true}
      else
        return render json: @user.userpic.errors, status: 422
      end
    else
      return render json: {error: 'Нет аватара'}, status: 404
    end
  end

  def change_email
  end

  def confirm_change_email
  end

  def change_password
    if @user.authenticate(params[:current_password])
      if @user.update_password(
            params[:password],
            params[:password_confirmation]
          )
        return render json: {success: true}
      else
        return render json: @user.errors, status: 422
      end
    else
      render json: {error: 'Неверный текущий пароль'}
    end
  end

  private
  def profile_params
    params.permit(:name, :birthdate, :gender, phones: [:number])
  end

  def user_params
    {
      name: params[:name],
      email: params[:email],
      password: params[:password],
      password_confirmation: params[:password_confirmation]
    }
  end
end