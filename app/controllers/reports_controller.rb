class ReportsController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:update, :create]
  def index
    get_reports
  end

  def show
    get_report
  end

  def create
    return unless get_room

    if @room.current_report
      return render json: { error: 'Показания за текущий период уже поданы' },
        status: :unprocessable_entity
    end

    @report = Report.new(report_params)
    @report.user = @user

    respond_to do |format|
      if @report.save
        format.json { render 'show', status: :ok }
      else
        format.json {
          render json: @report.errors, status: :unprocessable_entity
        }
      end
    end
  end

  def update
    get_report
    respond_to do |format|
      if @report.update(report_params)
        format.json { render nothing: true, status: :ok }
      else
        format.json {
          render json: @report.errors, status: :unprocessable_entity
        }
      end
    end
  end

  def destroy
    get_report
    respond_to do |format|
      if @report.destroy
        format.json { render nothing: true, status: :ok }
      else
        format.json { 
          render json: @report.errors, status: :unprocessable_entity
        }
      end
    end
  end

  private
  def get_reports
    if session[:user_type] === 'client'
      return @reports = @user.reports
    end

    if session[:user_type] === 'company'
      return @reports = @user.company.reports
    end
  end

  def get_room
    if params[:admin]
      @room = Room.find(params[:room_id])
    else
      @room = @user.rooms.find(params[:room_id])
    end
    unless @room.estate
      render json: {
        error: 'Для этого помещения нельзя подать показания' 
      }, status: 403

      return false
    end

    return @room

    rescue ActiveRecord::RecordNotFound
      render json: { 
          error: "Помещение с id=#{params[:room_id]} не найдено" 
        }, status: 404
  end

  def get_report
    if params[:admin]
      @report = Report.find(params[:id])
    else
      @report = @user.reports.find_by(id: params[:id])
    end

    unless @report
      render json: {
        error: "Не найден отчет с id=#{params[:id]}"
      }, status: 404
      return false
    end

    return @report
  end

  def report_params
    params.permit(:room_id, 
      report_entries_attributes: [
        :id,
        :counter_id,
        :value
      ]
    )
  end
end
