class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :get_user
  before_filter :check_permissions

  def get_user
    return if controller_name == 'templates'
    return if controller_name == 'password_reset'

    if session[:user]
      @user = User.find(session[:user])
    else
      if controller_name == 'users' && action_name == 'index' ||
          controller_name != 'users'
        respond_to do |format|
          format.json { render nothing: true, status: 403 }
          format.html { redirect_to '/login' }
        end
      end
    end
  rescue ActiveRecord::RecordNotFound
    session[:user] = nil
    redirect_to '/login'
  end

  def check_permissions
  end
end
