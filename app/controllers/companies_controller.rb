class CompaniesController < ApplicationController
  def update
    get_company()
    return unless @company

    if @company.verified
        # todo: create verification request
        return render json: {success: 'Создан запрос на обновление данных'},
            status: 202
    end

    if @company.update(company_params)
        render json: {success: true}, status: 200
    else
        render json: @company.errors, status: 422
    end
  end

  def upload_file
    get_company()

    if params[:file]
        if ['logo', 'innkpp', 'ogrn'].include? params[:type]
            if @company.set_file(params[:file], params[:type])
                return render json: {success: true}
            else
                return render json: @company.file_errors
            end
        else
            return render json: {
                error: 'Некорректный тип файла'
            }, status: 422
        end
    else
        return render json: {
            error: 'Файл не был отправлен'
        }, status: 404
    end
  end

  def delete_file
    get_company()

    unless ['logo', 'innkpp', 'ogrn'].include? params[:type]
        return render json: {
                error: 'Некорректный тип файла'
            }, status: 422
    end

    file = @company.get_file(params[:type])
    if file.present?
        if file.destroy
            return render json: {success: true}
        else
            return render json: file.errors, status: 422
        end
    else
        return render json: {error: 'Нет такого файла'}, status: 404
    end
  end

  def innkpp_file
    get_company()

    if @company.innkpp_file && File.exists?(@company.innkpp_file.filepath)

        extname = File.extname(@company.innkpp_file.filepath)[1..-1]
        mime_type = Mime::Type.lookup_by_extension(extname)
        content_type = unless mime_type.nil? then mime_type.to_s else 'text/plain' end

        send_file @company.innkpp_file.filepath, type: content_type, disposition: :inline
    else
        render json: {error: 'Файл не найден'}, status: 404
    end
  end

  def ogrn_file
    get_company()

    if @company.ogrn_file && File.exists?(@company.ogrn_file.filepath)
        extname = File.extname(@company.ogrn_file.filepath)[1..-1]
        mime_type = Mime::Type.lookup_by_extension(extname)
        content_type = unless mime_type.nil? then mime_type.to_s else 'text/plain' end

        send_file @company.ogrn_file.filepath, type: content_type, disposition: :inline
    else
        render json: {error: 'Файл не найден'}, status: 404
    end
  end

  private

  def get_company
    @company = @user.company
    unless @company
        return render json: {
            error: 'Пользователь не прикреплен к компании'
            }, status: 422
    end
  end

  def company_params
    params.permit(
            :company_name,
            :contact_person,
            :country,
            :region,
            :city,
            :address,
            :post_code,
            :inn,
            :kpp,
            :ogrn,
            :bank_account,
            :bik,
            :info,
            coordinates: [:lat, :lng],
            phones: [:number]
        )
  end
end
