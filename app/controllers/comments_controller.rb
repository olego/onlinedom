class CommentsController < ApplicationController
  def create
    @comment = Comment.new(comment_params)
    @comment.user_id = @user.id
    check_subscribers

    respond_to do |format|
      if @comment.save
        format.json { render :create, status: :created}
      else
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end
  private
    def check_subscribers
      if params[:commentable_type] == 'Ticket'
        ticket = Ticket.find_by(id: params[:commentable_id])
        ticket.update_subscriptions(@user) if ticket
      end
    end
    def comment_params
      params.permit(
        :commentable_id,
        :commentable_type,
        :body
      )
    end
end