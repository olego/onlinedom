class TariffsController < ApplicationController
  def list
    @tariffs = @user.company.tariffs
    render json: @tariffs
    # render json: Tariff.all
  end

  def show
    @tariff = @user.company.tariffs.find_by(id: params[:id])
    # @tariff = Tariff.find_by(id: params[:id])

    render json: @tariff
  end

  def names
    @enum_names = Tariff.names.keys

    render json: @enum_names 
  end

  def create
    @tariff = Tariff.new(tariff_params)

    if @tariff.save
      render json: {success: true}, status: :created 
    else
      render json: {
        error: "Ошибка создания тарифа",
        tariff_errors: @tariff.errors
      }, status: 422
    end
  end

  def update
  	# @tariff = Tariff.find_by(id: params[:id])
    @tariff = @user.company.tariffs.find_by(id: params[:id])
    if @tariff.update(tariff_params)
      render json: @tariff, status: :ok
    else
	    render json: {
	        error: "Ошибка обновления тарифа",
	        tariff_errors: @tariff.errors
	    }, status: 422
    end
  end

  def delete
    # @tariff = Tariff.find_by(id: params[:id])
    @tariff = @user.company.tariffs.find_by(id: params[:id])
    if @tariff.destroy
      render json: {success: true}, status: :ok
    else
      render json: {
          error: "Ошибка удаления тарифа",
          tariff_errors: @tariff.errors
      }, status: 422
    end
  end

  def archive
    # @archive_tariffs = ArchiveTariff.where(tariff_id: params[:id])
    @tariffs = @user.company.tariffs
    @archive_tariffs = []
    @tariffs.each do |tariff|
      @archive_tariffs << tariff.archive_tariffs
    end
    @archive_tariffs = @archive_tariffs.flatten.select { |num|  num.tariff_id == params[:id].to_i }
    
    render json: @archive_tariffs
  end

  private

  def tariff_params
    {
      name: params[:name],
      value: params[:value],
      estate_id: params[:estate_id]
    }
  end
end