class EstatesController < ApplicationController
  # GET /estates
  # GET /estates.json
  def index
    get_company
    @estates = @company.estates
  end

  # GET /estates/1
  # GET /estates/1.json
  def show
    get_company
  end

  # POST /estates
  # POST /estates.json
  def create
    get_company

    @estate = Estate.new(estate_params)
    @estate.estate_type = 'apartaments_house'
    @estate.company = @company
    respond_to do |format|
      if @estate.save
        set_rooms @estate.id
        format.json { render :show, status: :created }
      else
        format.json { render json: @estate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /estates/1
  # PATCH/PUT /estates/1.json
  def update
    get_company

    @estate.meta = params[:meta]
    respond_to do |format|
      if @estate.update(estate_params)
        format.json { render :show, status: :ok }
      else
        format.json { render json: @estate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /estates/1
  # DELETE /estates/1.json
  def destroy
    get_company

    respond_to do |format|
      if @estate.destroy
        format.json {
          render json: {success: true}, status: :ok    
        }
      else
        format.json {
          render json: @estate.errors,
            status: :unprocessable_entity
        }
      end
    end
  end

  def download
    @estate = Estate.includes(rooms: :reports).find(params[:id])
    
    # respond_to do |format|
    #   format.xls { send_data @estate.rooms.to_csv(params[:date], @estate.full_address) }
    # end
    render layout: false, disposition: 'inline', template: "estates/download.xlsx.axlsx", formats: [:xlsx]
  end

  def search
    @estates = Estate.where('full_address LIKE ?', "%#{params[:query]}%")
                     .limit(10)
    render 'search'
  end

  private
    def set_estate
      @estate = @company.estates.find_by(id: params[:id])
      set_rooms params[:id]
      return render json: {status: 'Объект не найден'}, status: 404 unless @estate
    end

    def estate_params
      params.permit(
        address: Estate.address_fields.keys,
        meta: [:housing_number, :nonresident_number],
        license: [:series, :number, :issue_date, :issued_by]
      )
    end

    def set_rooms id
      @rooms = Room.includes(:reports, :counters).actual.where(estate_id: id)
    end

    def get_company
      unless @company = @user.company
        return render json: {
          error: 'Пользователь не прикреплен к компании'
        }, status: 403
      end

      set_estate if params[:id]
    end
end
