class TemplatesController < ApplicationController
  def show
    unless params[:name]
      return render nothing: true, status: 404
    end

    if params[:subdir]
      template_name = "templates/#{params[:subdir]}/#{params[:name]}"
    else
      template_name = "templates/#{params[:name]}"
    end

    if template_exists? template_name
      render template_name, layout: false
    else
      render nothing: true, status: 404
    end
  end
end