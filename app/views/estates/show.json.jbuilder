json.extract! @estate,
    :id,
    :address,
    :estate_type,
    :license,
    :meta,
    :full_address,
    :short_address,
    :created_at, 
    :updated_at
json.rooms @rooms do |room|
  json.partial! 'estates/room', room: room
end
