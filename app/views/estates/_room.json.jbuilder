json.extract! room, 
    :id,
    :room_type,
    :address,
    :number,
    :company_name,
    :company_address,
    :company_phone,
    :meta,
    :map,
    :current_report_period

json.reports room.reports do |report|
    json.id report.id
    json.room_id report.room_id
    json.created_at report.created_at
    json.report_entries_attributes report.report_entries do |entry|
        json.id entry.id
        json.counter_id entry.counter.id
        json.counter_type entry.counter.counter_type
        json.counter_name entry.counter.name
        json.units entry.counter.units
        json.value entry.value
    end
end

if room.estate
    json.estate room.estate
    json.address room.estate.full_address
else
    json.address room.address
end

json.counters_attributes do
json.array! room.counters, 
    :id, 
    :place, 
    :counter_type,
    :regnum,
    :name,
    :precision,
    :seal_date,
    :seal_number,
    :replacement_date,
    :room_id,
    :units
end

if room.user
    json.owner do
        json.name room.user.name
        json.email room.user.email
        json.phones room.user.phones || ['']
        json.registered true
    end
else
    json.owner room.owner
end
