json.array! @employees do |employee|
    json.id employee.id
    json.phones employee.phones
    json.user do
        json.extract!(employee.user, :id,
          :user_type,
          :name,
          :email,
          :birthdate,
          :gender,
          :phones,
          :meta
        )
    end
    json.role employee.role
end