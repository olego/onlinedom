json.extract! @ticket, :id, :ticket_type, :status, :number, :meta, :created_at, :updated_at, :execute_at, :executor_id
json.room do
  json.extract! @ticket.room, :id, :number
  json.address @ticket.room.try(:estate).try(:full_address)
end
if session[:user_type] == 'company'
  json.executor @ticket.executor
end
json.user @ticket.user
json.editable @ticket.editable(@user)
json.comments @ticket.comments do |comment|
  json.partial! 'tickets/comment', comment: comment
end
