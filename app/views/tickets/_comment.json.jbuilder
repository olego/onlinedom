json.extract! comment, :id, :body
json.user do
  json.extract! comment.user, :id, :name
end