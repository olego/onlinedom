json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :ticket_type, :status, :number, :meta, :created_at, :updated_at, :execute_at
  json.room do
    json.extract! ticket.room, :id, :number
    json.address ticket.room.try(:estate).try(:full_address)
  end
  if session[:user_type] == 'company'
      json.executor ticket.executor
  end
  json.user ticket.user
  json.editable ticket.editable(@user)
  json.watched ticket.get_subscriptions(@user).try(:watched)
end
