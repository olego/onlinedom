json.extract! @room, 
    :id,
    :room_type,
    :address,
    :number,
    :company_name,
    :company_address,
    :company_phone,
    :meta,
    :map,
    :current_report_period

if @room.current_report
  json.current_report do
    json.extract! @room.current_report,
        :id,
        :room_id,
        :created_at,
        :report_entries
  end
else
    json.current_report false
end

  if @room.estate
    json.estate @room.estate
    json.address @room.estate.full_address
  else
    json.address @room.address
  end

json.counters_attributes do
json.array! @room.counters, 
    :id, 
    :place, 
    :counter_type,
    :regnum,
    :name,
    :precision,
    :seal_date,
    :seal_number,
    :replacement_date,
    :room_id,
    :units
end

if @room.user
    json.owner do
        json.name @room.user.name
        json.email @room.user.email
        json.phones @room.user.phones || ['']
        json.registered true
    end
else
    json.owner @room.owner
end