json.array!(@reports) do |report|
    json.id report.id
    json.room_id report.room.id
    json.room_address report.room.address
    json.owner_id report.room.user.id
    json.owner_name report.room.user.name

    json.report_entries_attributes do
        json.array!(report.report_entries) do |entry|
            json.counter_id entry.counter.id
            json.counter_name entry.counter.name
            json.value entry.value
        end
    end
end