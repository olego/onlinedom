angular
    .module('apiCompany', [])
    .factory('apiCompany', ['$http', function($http) {
        //var urlBase = '/api/customers';
        var service = {
            report: {
                update: function(params){
                    return $http({
                        url: 'api/reports/' + params.id,
                        method: "PUT",
                        data: params
                    });
                },
                create: function(params){
                    return $http({
                        url: 'api/reports/',
                        method: "POST",
                        data: params
                    });
                }
            },
            estate: {
                download: function(params){
                    return $http({
                        url: 'api/estates/'+ params.id + '/download.xls?date=' + params.report_date,
                        method: "GET",
                        data: params
                    });
                }
            },
            comment: {
                send: function(params){
                    return $http({
                        url: 'api/comments/',
                        method: "POST",
                        data: params
                    });
                }
            }
        };

        service.getCompanyInfo = function(){
            return $http({
                url: 'api/users/me/',
                method: "GET"
            })
        };

        service.updateCompanyInfo = function(params){
            var data = {
                contact_person: params.contact_person,
                phones: params.phones,
                country: params.country,
                region: params.region,
                city: params.city,
                address: params.address,
                post_code: params.post_code,
                inn: params.inn,
                kpp: params.kpp,
                ogrn: params.ogrn,
                bank_account: params.bank_account,
                bik: params.bik,
                info: params.info,
                coordinates: {
                    lat: params.lat,
                    lng: params.lng
                },
                company_name: params.company_name,
                authenticity_token: params.authenticity_token
            };
            return $http({
                url: 'api/companies/update/',
                method: "POST",
                data: data
            });
        };

        service.uploadImage = function(type, file, token){
            var formData = new FormData();
            formData.append("file", file);
            formData.append('authenticity_token', token);
            return $http.post('api/companies/upload_file/' + type, formData, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

        service.deleteImage = function(type, token){
            return $http.delete('api/companies/delete_file/' + type,
                {
                    params: {
                        'authenticity_token': token
                    }
                });
        };

        // Company Tariffs
        service.addTariff = function(params){
            return $http({
                url: 'api/tariffs/',
                method: "POST",
                data: params
            })
        };

        service.updateTariff = function(params){
            return $http({
                url: 'api/tariffs/' + params.id,
                method: "PUT",
                data: params
            });
        };

        service.deleteTariff = function(id, token){
            return $http.delete('api/tariffs/' + id,
                {
                    params: {
                        'authenticity_token': token
                    }
                });
        };

        // Company Employees
        service.addEmployee = function(params){
            return $http({
                url: 'api/employees/',
                method: "POST",
                data: params
            })
        };

        service.getEmployeesList = function(){
            return $http({
                url: 'api/employees/list.json',
                method: "GET"
            })
        };

        service.deleteEmployee = function(id, token){
            return $http.delete('api/employees/' + id,
                {
                    params: {
                        'authenticity_token': token
                    }
                });
        };

        service.getEmployeesInfo = function(id){
            return $http({
                url: 'api/employees/' + id + '.json',
                method: "GET"
            });
        };

        service.updateEmployee = function(params){
            return $http({
                url: 'api/employees/' + params.id,
                method: "PUT",
                data: params
            });
        };

        // Company Roles
        service.addRole = function(params){
            return $http({
                url: 'api/roles/',
                method: "POST",
                data: params
            })
        };

        service.deleteRole = function(id, token){
            return $http.delete('api/roles/' + id,
                {
                    params: {
                        'authenticity_token': token
                    }
                });
        };

        service.updateRole = function(params){
            return $http({
                url: 'api/roles/' + params.id,
                method: "PUT",
                data: params
            });
        };

        // Company Estates
        service.addOrUpdateEstate = function(params){
          if(params.id){
            return $http({
                url: 'api/estates/' + params.id,
                method: "PUT",
                data: params
            });
          }else{
            return $http({
                url: 'api/estates/',
                method: "POST",
                data: params
            })
          }
        };

        service.deleteEstate = function(id, token){
            return $http.delete('api/estates/' + id,
                {
                    params: {
                        'authenticity_token': token
                    }
                });
        };

        service.uploadLicense = function(file, token, estate_id){
            var formData = new FormData();
            formData.append("file", file);
            formData.append('authenticity_token', token);
            return $http.post('api/estates/' + estate_id + '/file/license', formData, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

        service.deleteLicense = function(token, estate_id){
            return $http.delete('api/estates/' + estate_id + '/file/license',
                {
                    params: {
                        'authenticity_token': token
                    }
                });
        };

        service.request = {
            add: function(params){
                return $http({
                    url: 'api/tickets/',
                    method: "POST",
                    data: params
                });
            },
            edit: function(params){
                return $http({
                    url: 'api/tickets/' + params.id,
                    method: "PUT",
                    data: params
                });
            },
            delete: function(id, token){
                return $http.delete('/api/tickets/' + id,
                {
                    params: {
                        'authenticity_token': token
                    }
                });
            }
        };

        service.rooms = {
            update: function(params){
                return $http({
                    url: 'api/rooms/' + params.id,
                    method: "PATCH",
                    data: params
                });
            }
        }

        service.mode = {
            change: function(token){
                return $http({
                    url: '/users/mode/client',
                    method: "PUT",
                    params: {
                        'authenticity_token': token
                    }
                });
            }
        }

        return service;
    }]);
