angular
    .module('apiService', [])
    .factory('apiService', ['$http', function($http) {
        //var urlBase = '/api/customers';
        var service = {};

        service.registerNewUser = function(params){
            return $http({
                url: 'users',
                method: "POST",
                data: params
            })
        };

        service.login = function(params){
            return $http({
                url: 'login',
                method: "POST",
                data: {
                    'email' : params.email,
                    'password' : params.password,
                    'authenticity_token' : params.authenticity_token
                }
            })
        };
        service.restorePassword = function(params){
            return $http({
                url: 'reset',
                method: "POST",
                data: {
                    'email' : params.email,
                    'authenticity_token' : params.authenticity_token
                }
            })
        };
        service.resetPassword = function(params){
            return $http({
                url: 'reset/' + params.key,
                method: "POST",
                data: {
                    'password' : params.password,
                    'password_confirmation' : params.password_conf,
                    'authenticity_token' : params.authenticity_token
                }
            })
        };

        service.getCompanyInfo = function(){
            return $http({
                url: 'api/users/me/',
                method: "GET"
            })
        };

        service.updateCompanyInfo = function(params){
            var data = {
                contact_person: params.contact_person,
                phones: params.phones,
                country: params.country,
                region: params.region,
                city: params.city,
                address: params.address,
                post_code: params.post_code,
                inn: params.inn,
                ogrn: params.ogrn,
                bank_account: params.bank_account,
                bik: params.bik,
                info: params.info,
                coordinates: {
                    lat: params.lat,
                    lng: params.lng
                },
                authenticity_token: params.authenticity_token
            };
            return $http({
                url: 'api/companies/update/',
                method: "POST",
                data: data
            });
        };

        service.uploadImage = function(type, file, token){
            var formData = new FormData();
            formData.append("file", file);
            formData.append('authenticity_token', token);
            return $http.post('api/companies/upload_file/' + type, formData, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

        service.deleteImage = function(type, token){
            return $http.delete('api/companies/delete_file/' + type,
                {
                    params: {
                        'authenticity_token': token
                    }
                });
        };

        // Company Employees
        service.addEmployee = function(params){
            return $http({
                url: 'api/employees/',
                method: "POST",
                data: params
            })
        };

        service.getEmployeesList = function(){
            return $http({
                url: 'api/employees/list.json',
                method: "GET"
            })
        };

        service.deleteEmployee = function(id, token){
            return $http.delete('api/employees/' + id,
                {
                    params: {
                        'authenticity_token': token
                    }
                });
        };

        service.getEmployeesInfo = function(id){
            return $http({
                url: 'api/employees/' + id + '.json',
                method: "GET"
            });
        };

        service.updateEmployee = function(params){
            return $http({
                url: 'api/employees/' + params.id,
                method: "PUT",
                data: params
            });
        };

        // Company Roles
        service.addRole = function(params){
            return $http({
                url: ' api/roles/',
                method: "POST",
                data: params
            })
        };

        service.deleteRole = function(id, token){
            return $http.delete('api/roles/' + id,
                {
                    params: {
                        'authenticity_token': token
                    }
                });
        };

        service.updateRole = function(params){
            return $http({
                url: 'api/roles/' + params.id,
                method: "PUT",
                data: params
            });
        };

        // User APIs
        service.uploadUserPhoto = function(file, token){
            var formData = new FormData();
            formData.append("file", file);
            formData.append('authenticity_token', token);
            return $http.post('api/users/upload_userpic/', formData, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        };

        service.deleteUserPhoto = function(token){
            return $http.delete('api/users/delete_userpic',
                {
                    params: {
                        'authenticity_token': token
                    }
                });
        };

        service.getUserInfo = function(){
            return $http({
                url: 'api/users/me/',
                method: "GET"
            })
        };

        service.updateUserInfo = function(params){
            return $http({
                    url: 'api/users/profile/',
                    method: "POST",
                    data: params
                });
        };

        // User rooms APIs
        service.getRoomsList = function(){
            return $http({
                    url: 'api/rooms',
                    method: "GET"
                })
        };

        service.addRoom = function(params, description){
            var desc = angular.toJson(description);
            params.description = desc;
            return $http({
                url: 'api/rooms/',
                method: "POST",
                data: params
            });
        };
        return service;
    }]);
