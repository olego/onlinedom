angular.module('client').directive('counterList', [function () {
    return {
        restrict: 'E',
        scope: {
          list: '=',
          mode: '='
        },
        templateUrl: 'template/counter-list',
        link: function(scope, element, attrs)
           {
               scope.initCounterModel = function(){
                   scope.counterModel = {
                       place: 'Кухня',
                       counter_type: 'Газ',
                       regnum: '',
                       name: '',
                       precision: '',
                       seal_date: new Date(),
                       seal_number: '',
                       replacement_date: new Date()
                   }
                   scope.selectedCounterIndex = '';
                   scope.isEditMode = false;
               };
               scope.initCounterModel();
               scope.addCounter = function(){
                   scope.list.push(scope.counterModel);
                   scope.initCounterModel();
                   $('#counter-dialog').modal('hide');
               };
               scope.setSelectedCounterData = function(index){
                   scope.isEditMode = true;
                   scope.setSelectedCounterIndex(index);
                   scope.counterModel = scope.list[index];
               };
               scope.setSelectedCounterIndex = function(index){
                   scope.selectedCounterIndex = index;
               };
               scope.updateCounter = function(){
                   scope.list[scope.selectedCounterIndex] = scope.counterModel;
                   scope.initCounterModel;
                   $('#counter-dialog').modal('hide');
               }
               scope.deleteSelectedCounter = function(index){
                   if(scope.mode == "add"){
                       scope.list.splice(index, 1);
                   }else{
                       scope.list[index]._destroy = true;
                   }
                   $('#remove-dialog').modal('hide');
               }
           }
    }
}]);
