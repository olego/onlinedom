angular.module('client').directive('alert',['alertService', function (alertService) {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'template/login-alert',
        link: {
            pre: function(scope) {
                scope.alert = alertService;
            }
        }
    }
}]);