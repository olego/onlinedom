angular.module('client').controller('addRoomController', ['$scope', 'apiClient', 'alertService', '$location', '$routeParams',
    function ($scope, apiClient, alertService, $location, $routeParams) {
            $scope.init = function () {
                $scope.roomModel = {
                    room_type: "Квартира",
                    address: '',
                    number: '',
                    company_name: '',
                    company_address: '',
                    company_phone: '',
                    estate_id: '',
                    map: {
                        lat: '',
                        lng: ''
                    },
                    counters_attributes: [],
                    meta: {
                        beautification: {
                            rooms_ammount: 1,
                            general_area: '',
                            balcony_area: '',
                            floor: '',
                            bathroom: '',
                            property_rigth: '',
                            convenience: {
                                gas: '',
                                cold_water: '',
                                warn_water: '',
                                electricity: '',
                                heating: '',
                                sewage: ''
                            }
                        },
                        tenants: []
                    },
                    authenticity_token: window.__token
                };
                $scope.workMode = 'add';
                $scope.showSpinner = false;
                $scope.addresses = {};
                $scope.showAddEstateForm = false;
            };

        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiClient.room.add($scope.roomModel).then(function(){
                $scope.showSpinner = false;
                alertService.updateAlert('success', 'Помещение успешно добавлено');
                $location.path('/rooms');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            });
        };
        $scope.addTenant = function(){
            $scope.roomModel.meta.tenants.push({
                    name: '',
                    mode: '',
                    status: '',
                    rights: ''
            });
        };
        $scope.removeTenant = function(index){
            $scope.roomModel.meta.tenants.splice(index, 1);
        };
        $scope.setCompanyInfo = function(item){
            $scope.roomModel.address = item.full_address;
            if (item.company) {
                $scope.roomModel.company_name = item.company.company_name;
                $scope.roomModel.company_address = item.company.country + ", " + item.company.region + ", г. " + item.company.city + ", " + item.company.address;
                $scope.roomModel.company_phone = item.company.phones[0].number;
            }
            $scope.roomModel.estate_id = item.id;
        }
        $scope.refreshAddresses = function(str) {
            if(str.length > 0){
                apiClient.estates.find(str).then(function(res){
                    $scope.addresses = res.data;
                }, function(res){
                    alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                })
            }
        };
    }]);
