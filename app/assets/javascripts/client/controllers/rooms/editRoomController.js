angular.module('client').controller('editRoomController', ['$scope', 'apiClient', 'alertService', '$location', '$routeParams', "room",
    function ($scope, apiClient, alertService, $location, $routeParams, room) {
        $scope.init = function () {
                $scope.roomModel = room.data;
                $scope.roomModel.authenticity_token = window.__token;
                if($scope.roomModel.meta.tenants == null){
                    $scope.roomModel.meta.tenants = [];
                }
                $scope.showAddEstateForm = $scope.roomModel.company_name ? false : true;
                console.log($scope.roomModel);
            };
            $scope.workMode = 'edit';
            $scope.showSpinner = false;

        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiClient.room.edit($scope.roomModel).then(function(){
                $scope.showSpinner = false;
                alertService.updateAlert('success', 'Помещение успешно добавлено');
                $location.path('/rooms');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            })
        };
        $scope.addTenant = function(){
            $scope.roomModel.meta.tenants.push({
                    name: '',
                    mode: '',
                    status: '',
                    rights: ''
            });
        };
        $scope.removeTenant = function(index){
            $scope.roomModel.meta.tenants.splice(index, 1);
        };

        $scope.$on('mapInitialized', function(evt, evtMap){
            $scope.types = "['address']";
            var map = evtMap;
            var marker = new google.maps.Marker({map: map});
            if($scope.roomModel.map.lat && $scope.roomModel.map.lng){
                $scope.map.setCenter($scope.roomModel.map);
                marker.setPosition(
                    {
                        lat: Number($scope.roomModel.map.lat),
                        lng: Number($scope.roomModel.map.lng)}
                );
            }
            geocoder = new google.maps.Geocoder();
            $scope.placeChanged = function() {
                $scope.place = this.getPlace();
                $scope.map.setCenter($scope.place.geometry.location);
                marker.setPosition($scope.place.geometry.location);
                $scope.setModelCoordinates($scope.place.geometry.location.lat(), $scope.place.geometry.location.lng());

            };
            $scope.getCoordinates = function(e){
                $scope.getAddress(e.latLng);
                marker.setPosition(e.latLng);
                $scope.setModelCoordinates(e.latLng.lat(), e.latLng.lng());
            };
            $scope.setModelCoordinates = function(lat, lng){
                $scope.roomModel.map.lat = lat;
                $scope.roomModel.map.lng = lng;
            };
            $scope.getAddress = function(latLng) {
                geocoder.geocode( {'latLng': latLng},
                    function(results, status) {
                        if(status == google.maps.GeocoderStatus.OK) {
                            if(results[0] && results[0].formatted_address.indexOf("Unnamed Road") == -1) {
                                $scope.roomModel.address = results[0].formatted_address;
                            }
                            else {
                                console.log('error');
                            }
                        }
                        else {
                            console.log('error: ' + status);
                        }
                    });
            }
        });

    }]);
