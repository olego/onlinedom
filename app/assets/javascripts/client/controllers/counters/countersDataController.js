angular.module('client').controller('сountersDataController', ['$scope', 'apiClient', 'alertService', '$location', 'room',
    function ($scope, apiClient, alertService, $location, room) {
        $scope.init = function() {
                $scope.roomModel = room.data;
                $scope.showSpinner = false;
                $scope.reportsModel = {
                    room_id: $scope.roomModel.id,
                    report_entries_attributes: [],
                    authenticity_token: window.__token
                };
                $scope.populateReportsModel();
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiClient.report.send($scope.reportsModel).then(function(){
                $location.path('/counters');
                alertService.updateAlert('success', 'Показания отправлены успешно');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            })
        }
        $scope.populateReportsModel = function(){
            for(var i = 0; i < $scope.roomModel.counters_attributes.length; i++){
                var obj = {
                    counter_id: $scope.roomModel.counters_attributes[i].id
                };
                $scope.reportsModel.report_entries_attributes.push(obj);
            }
        }
    }]);
