angular.module('client').controller('countersController', ['$scope', 'apiClient', 'alertService', 'list',
    function ($scope, apiClient, alertService, list) {
        $scope.init = function() {
            $scope.roomsModel = {
                rooms: list.data,
            };
            $scope.roomsAmount = list.data.length;
            $scope.authenticity_token = window.__token;
            if($scope.roomsAmount > 0){
                $scope.roomsSafe = angular.copy($scope.roomsModel.rooms);
            }
        };
        $scope.setObjIdToRemove = function(id, index){
            $scope.selectedObjId = id;
            $scope.selectedObjIndex = index;
        };
        $scope.deleteSelectedObj = function(id, index){
            $scope.buttonDisabled = true;
            apiClient.room.delete($scope.selectedObjId, $scope.authenticity_token).then(function(){
                $scope.buttonDisabled = false;
                $('#remove-dialog').modal('hide');
                $scope.roomsModel.rooms.splice(index, 1);
                alertService.updateAlert('success', 'Помещение удалено');
            }, function(){
                $scope.buttonDisabled = false;
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            })
        };
    }]);
