angular.module('client').controller('editRequestController', ['$scope', '$location', 'apiClient', 'alertService', 'request',
    function ($scope, $location, apiClient, alertService, request) {
        $scope.init = function() {
            $scope.room = request.data.room;
            $scope.requestModel = {
                id: request.data.id,
                room_id: $scope.room.id,
                ticket_type: request.data.ticket_type,
                meta: {
                    description: request.data.meta.description
                },
                authenticity_token: window.__token
            };

            $scope.showSpinner = false;
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiClient.request.edit($scope.requestModel).then(function(){
                $scope.showSpinner = false;
                alertService.updateAlert('success', 'Заявка успешно обновлена');
                $location.path('/requests');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            });
        }
    }]);
