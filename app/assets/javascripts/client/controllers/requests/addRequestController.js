angular.module('client').controller('addRequestController', ['$scope', '$location', 'apiClient', 'alertService', 'rooms',
    function ($scope, $location, apiClient, alertService, rooms) {
        $scope.init = function() {
            $scope.rooms = rooms.data;
            $scope.requestModel = {
                room_id: $scope.rooms[0].id,
                ticket_type: 'Обычная',
                meta: {
                    description: ''
                },
                authenticity_token: window.__token
            }
            $scope.showSpinner = false;
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiClient.request.add($scope.requestModel).then(function(){
                $scope.showSpinner = false;
                alertService.updateAlert('success', 'Заявка успешно заведена');
                $location.path('/requests');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            });
        }

        $scope.beforeRender = function($dates){
            var minDate = new Date();
            for(d in $dates){         
                if($dates[d].utcDateValue<minDate){
                    $dates[d].selectable = false
                }
            }
        }
    }]);
