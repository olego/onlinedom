angular.module('client').controller('showRequestController', ['$scope', '$location', 'apiClient', 'alertService', 'request',
    function ($scope, $location, apiClient, alertService, request) {
        $scope.init = function() {
            $scope.requestModel = request.data;
            $scope.requestModel.authenticity_token = window.__token;
            console.log($scope.requestModel);
            $scope.commentModel = {
              body: '',
              commentable_id: request.data.id,
              commentable_type: 'Ticket',
              authenticity_token: window.__token
            };
            if (request.data.status == "Исполнено")
                $scope.requestModel.finished = true;
            if (request.data.status == "Создана")
                $scope.requestModel.new = true;
            if (request.data.execute_at)
                $scope.requestModel.execute_at = request.data.execute_at;
        };

        $scope.sendComment = function(){
            $scope.showSpinner = true;

            apiClient.comment.send($scope.commentModel).then(function(response){
                $scope.showSpinner = false;
                $scope.requestModel.comments.push(response.data)
                $scope.commentModel.body = '';
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            });
        }
        $scope.rejectRequest = function(){
            $scope.showSpinner = true;
            var status = "Создана";
            changeStatus(status);
        }
        $scope.approveRequest = function(){
            $scope.showSpinner = true;
            var status = "Закрыта";
            changeStatus(status);
        }

        function changeStatus(status){
            $scope.requestModel.status = status;

            apiClient.request.changeStatus($scope.requestModel).then(function(){
                $scope.showSpinner = false;
                alertService.updateAlert('success', 'Заявка возвращена');
                $location.path('/requests');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            });
        }
    }]);
