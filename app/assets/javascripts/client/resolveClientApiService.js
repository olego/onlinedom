angular.module('client')
    .factory('resolveClientApiService', ['$http', function($http) {
        var service = {
            getRoomsList: function(){
                return $http({
                    url: 'api/rooms',
                    method: "GET"
                })
            },

            getRoom: function(id){
                return $http({
                    url: 'api/rooms/' + id,
                    method: "GET"
                })
            },

            getRequestsList: function(){
                return $http({
                    url: 'api/tickets/',
                    method: "GET"
                })
            },

            getRequest: function(id){
                return $http({
                    url: 'api/tickets/' + id,
                    method: "GET"
                })
            },
        }

        return service;
    }]);
