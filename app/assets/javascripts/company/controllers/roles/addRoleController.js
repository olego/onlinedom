angular.module('company').controller('addRoleController', ['$scope', 'apiCompany', 'alertService', '$location', 'rolesDesc',
    function ($scope, apiCompany, alertService, $location, rolesDesc) {
        $scope.init = function(){
            $scope.rolesDesc = rolesDesc.data;
            $scope.roleModel = {
                name: '',
                permission: [],
                authenticity_token: window.__token
            };
            $scope.showSpinner = false;
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiCompany.addRole($scope.roleModel).then(function(){
                alertService.updateAlert('success', 'Роль добавлена успешно');
                $scope.showSpinner = false;
                $location.path('/roles');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            })
        };
    }]);