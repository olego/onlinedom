angular.module('company').controller('rolesController', ['$scope', 'apiCompany', 'alertService', '$location', 'rolesList',
    function ($scope, apiCompany, alertService, $location, rolesList) {
        $scope.init = function() {
            $scope.roleModel = {
                rolesList: [],
                rolesAmount: 0,
                authenticity_token: window.__token
            };
            $scope.roleModel.rolesList = $scope.roleModel.rolesList.concat(rolesList.data);
            $scope.roleModel.rolesAmount = rolesList.data.length;
            if($scope.roleModel.rolesAmount > 0){
                $scope.rolesSafe = angular.copy($scope.roleModel.rolesList);
            }
        };
        $scope.setRoleIdToRemove = function(id, index){
            $scope.selectedRoleId = id;
            $scope.selectedRoleIndex = index;
        };
        $scope.deleteSelectedRole = function(id, index){
            $scope.buttonDisabled = true;
            apiCompany.deleteRole(id, $scope.roleModel.authenticity_token).then(function(){
                $scope.buttonDisabled = false;
                $('#remove-dialog').modal('hide');
                $scope.roleModel.rolesList.splice(index, 1);
                alertService.updateAlert('success', 'Сотрудник удален');
            }, function(){
                $scope.buttonDisabled = false;
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            })
        };
    }]);