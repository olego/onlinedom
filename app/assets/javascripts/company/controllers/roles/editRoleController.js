angular.module('company').controller('editRoleController', ['$scope', 'apiCompany', 'alertService', '$location', 'roleData',
    function ($scope, apiCompany, alertService, $location, roleData) {
        $scope.init = function(){
            $scope.roleModel = roleData.data;
            $scope.roleModel.authenticity_token = window.__token;
            $scope.showSpinner = false;
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            console.log($scope.roleModel);
            apiCompany.updateRole($scope.roleModel).then(function(){
                alertService.updateAlert('success', 'Роль успешно обновлена');
                $scope.showSpinner = false;
                $location.path('/roles');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            })
        };
    }]);