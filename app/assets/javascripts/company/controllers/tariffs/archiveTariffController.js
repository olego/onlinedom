angular.module('company').controller('archiveTariffController', ['$scope', 'apiCompany', 'alertService', '$location', 'archiveTariffsList', 'tariffData',
    function ($scope, apiCompany, alertService, $location, archiveTariffsList, tariffData) {
        $scope.init = function() {
            $scope.tariffsModel = {
                archiveTariffsList: archiveTariffsList.data,
                archiveAmount: archiveTariffsList.data.length,
                authenticity_token: window.__token,
                archiveTariffName: tariffData.data.name
            };
            // set value tariff name
            // $scope.estatesList.forEach(function (estate) {
            // $scope.tariffsModel.archiveTariffsList.forEach(function (archiveTariff) {
            //     if (estate.id === archiveTariff.tariff_id) {
            //         archiveTariff.name = estate.name;
            //     }
            // });
            // });
        };
    }]);