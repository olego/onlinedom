angular.module('company').controller('addTariffController', ['$scope', 'apiCompany', 'alertService', '$location', 'estatesList', 'tariffNamesList',
    function ($scope, apiCompany, alertService, $location, estatesList, tariffNamesList) {
        $scope.init = function() {
            $scope.estatesList = estatesList.data;
            $scope.tariffNames = tariffNamesList.data;
            $scope.tariffModel = {
                name: '',
                value: '',
                estate_id: '',
                full_address: 'Выберите объект',
                role_id: '',
                authenticity_token: window.__token
            };
            $scope.showSpinner = false;
        };
        $scope.selectEstate = function(estateId, estateFullAddress){
            $scope.tariffModel.estate_id = estateId;
            $scope.tariffModel.full_address = estateFullAddress;
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiCompany.addTariff($scope.tariffModel).then(function(){
                alertService.updateAlert('success', 'Тариф добавлен успешно');
                $scope.showSpinner = false;
                $location.path('/tariffs');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            })
        }

    }]);