angular.module('company').controller('tariffsController', ['$scope', 'apiCompany', 'alertService', '$location', 'objectsList', 'estatesList',
    function ($scope, apiCompany, alertService, $location, objectsList, estatesList) {
        $scope.init = function() {
            $scope.estatesList = estatesList.data;
            $scope.tariffsModel = {
                tariffsList: objectsList.data,
                tariffsAmount: 0,
                authenticity_token: window.__token
            };
            $scope.tariffsModel.tariffsAmount = objectsList.data.length;

            // set default value full_address for tariff object
            $scope.estatesList.forEach(function (estate) {
                $scope.tariffsModel.tariffsList.forEach(function (tariff) {
                    if (estate.id === tariff.estate_id) {
                        tariff.full_address = estate.full_address;
                    }
                });
            });
        };
        $scope.setObjIdToRemove = function(id, index){
            $scope.selectedObjId = id;
            $scope.selectedObjIndex = index;
        };
        $scope.deleteSelectedTariff = function(id, index) {
            $scope.buttonDisabled = true;
            apiCompany.deleteTariff(id, $scope.tariffsModel.authenticity_token).then(function(){
                $scope.buttonDisabled = false;
                $('#remove-dialog').modal('hide');
                $scope.tariffsModel.tariffsList.splice(index, 1);
                alertService.updateAlert('success', 'Тариф удален');
            }, function(){
                $scope.buttonDisabled = false;
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            })
        };
    }]);