angular.module('company').controller('editTariffController', ['$scope', 'apiCompany', 'alertService', '$location', '$routeParams', 'estatesList', 'tariffData', 'tariffNamesList',
    function ($scope, apiCompany, alertService, $location, $routeParams, estatesList, tariffData, tariffNamesList) {
        $scope.init = function(){
            $scope.estatesList = estatesList.data;
            $scope.tariffNames = tariffNamesList.data;
            $scope.tariffModel = {
                id: tariffData.data.id,
                name: tariffData.data.name,
                value: tariffData.data.value,
                estate_id: tariffData.data.estate_id,
                authenticity_token: window.__token
            };
            // set default value full_address for estate object
            $scope.estatesList.forEach(function (estate) {
              if (estate.id === $scope.tariffModel.estate_id) {
                $scope.tariffModel.full_address = estate.full_address;
              }
            });

            $scope.showError = false;
            $scope.showSpinner = false;
        };
        $scope.selectEstate = function(estateId, estateFullAddress){
            $scope.tariffModel.estate_id = estateId;
            $scope.tariffModel.full_address = estateFullAddress;
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiCompany.updateTariff($scope.tariffModel).then(function(){
                alertService.updateAlert('success', 'Информация о тарифе обновлена');
                $location.path('/tariffs');
                $scope.showSpinner = false;
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            })
        }
    }]);
