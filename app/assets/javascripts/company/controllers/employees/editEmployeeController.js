angular.module('company').controller('editEmployeeController', ['$scope', 'apiCompany', 'alertService', '$location', '$routeParams', 'rolesList', 'empData',
    function ($scope, apiCompany, alertService, $location, $routeParams, rolesList, empData) {
        $scope.init = function(){
            $scope.rolesList = rolesList.data;
            $scope.empModel = {
                name: empData.data.user.name,
                phones: empData.data.phones,
                id: empData.data.id,
                authenticity_token: window.__token
            };
            if(empData.data.role){
                $scope.empModel.roleName = empData.data.role.name;
                $scope.empModel.role_id= empData.data.role.id;
            }
            $scope.showError = false;
            $scope.showSpinner = false;
            // TODO удалить, пока пусть будет
            //var id = $routeParams.empId;
            //if(id){
            //    apiCompany.getEmployeesInfo(id).then(function(response){
            //        var data = response.data;
            //        $scope.empModel.name = data.user.name;
            //        $scope.empModel.phones = data.phones;
            //        $scope.empModel.id = data.id;
            //        $scope.empModel.authenticity_token = window.__token;
            //
            //    }, function(){
            //        $scope.showError = true;
            //    });
            //}else{
            //    $scope.showError = true;
            //}
        };
        $scope.addPhone = function(){
            $scope.empModel.phones.push(
                {
                    number: ''
                }
            );
        };
        $scope.removePhone = function(index){
            $scope.empModel.phones.splice(index, 1);
        };
        $scope.selectRole = function(roleName, roleId){
            $scope.empModel.roleName = roleName;
            $scope.empModel.role_id = roleId;
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiCompany.updateEmployee($scope.empModel).then(function(){
                alertService.updateAlert('success', 'Информация о сотрунике обновлена');
                $location.path('/employees');
                $scope.showSpinner = false;
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            })
        }
    }]);
