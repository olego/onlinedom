angular.module('company').controller('employeesController', ['$scope', 'apiCompany', 'alertService', '$location', 'employeesList',
    function ($scope, apiCompany, alertService, $location, employeesList) {
        $scope.init = function() {
            $scope.displayedCollection = [];
            $scope.empModel = {
                empAmount: 0,
                authenticity_token: window.__token,
                employees: []
            };
            $scope.buttonDisabled = false;
            $scope.selectedUserId = 0;
            $scope.selectedUserIndex = 0;
            var data = employeesList.data;
            $scope.empModel.empAmount = data.length;
            if ($scope.empModel.empAmount > 0) {
                data.forEach(function (emp) {
                    $scope.empModel.employees.push(emp);
                });

                // for smart-tables
                $scope.employeesSafe
                    = angular.copy($scope.empModel.employees);
            }
            // TODO обработка ошибки
            //$scope.empModel.empAmount = 0;
        };

        $scope.setEmpIdToRemove = function(id, index){
            $scope.selectedUserId = id;
            $scope.selectedUserIndex = index;
        };
        $scope.deleteSelectedEmp = function(id, index){
            $scope.buttonDisabled = true;
            apiCompany.deleteEmployee(id, $scope.empModel.authenticity_token).then(function(){
                $scope.buttonDisabled = false;
                $('#remove-dialog').modal('hide');
                $scope.empModel.employees.splice(index, 1);
                alertService.updateAlert('success', 'Сотрудник удален');
            }, function(){
                $scope.buttonDisabled = false;
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            })
        };
    }]);