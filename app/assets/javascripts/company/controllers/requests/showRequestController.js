angular.module('company').controller('showRequestController', ['$scope', '$location', 'apiCompany', 'alertService', 'request',
    function ($scope, $location, apiCompany, alertService, request) {
        $scope.init = function() {
            $scope.requestModel = request.data;
            console.log($scope.requestModel);
            $scope.commentModel = {
              body: '',
              commentable_id: request.data.id,
              commentable_type: 'Ticket',
              authenticity_token: window.__token
            };
        };
        $scope.sendComment = function(){
            $scope.showSpinner = true;

            apiCompany.comment.send($scope.commentModel).then(function(response){
                $scope.showSpinner = false;
                $scope.requestModel.comments.push(response.data)
                $scope.commentModel.body = '';
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            });
        }
    }]);
