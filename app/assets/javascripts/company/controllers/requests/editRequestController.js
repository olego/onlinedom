angular.module('company').controller('editRequestController', ['$scope', '$location', 'apiCompany', 'alertService', 'request', 'employees',
    function ($scope, $location, apiCompany, alertService, request, employees) {
        $scope.init = function() {
            $scope.requestModel = request.data;
            if($scope.requestModel.executor){
                $scope.requestModel.executor_id = $scope.requestModel.executor.id;
            }
            $scope.employees = employees.data;
            console.log($scope.employees);
            $scope.requestStatuses = [
                'Создана',
                'Просмотрена',
                'В работе',
                'Отклонено',
                'Исполнено'
            ];
            $scope.requestModel.authenticity_token = window.__token;
            $scope.showSpinner = false;
        };
        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiCompany.request.edit($scope.requestModel).then(function(){
                $scope.showSpinner = false;
                alertService.updateAlert('success', 'Заявка успешно обновлена');
                $location.path('/requests');
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            });
        }
    }]);
