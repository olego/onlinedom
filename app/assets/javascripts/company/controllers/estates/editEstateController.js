angular.module('company').controller('editEstateController', ['$scope', '$timeout', 'apiCompany', 'alertService', '$location', '$routeParams', 'objectData',
    function ($scope, $timeout, apiCompany, alertService, $location, $routeParams, objectData) {
        $scope.init = function(){
            $scope.estateModel = objectData.data;
            $scope.estateModel.authenticity_token = window.__token;
            $scope.showSpinner = false;
            $scope.initSupportModel();
            $scope.spinnersState = {
                uploadSpinner: false,
                deleteSpinner: false
            }
        };

        $scope.submitForm = function(){
            $scope.showSpinner = true;
            apiCompany.updateEstate($scope.estateModel).then(function(){
                alertService.updateAlert('success', 'Информация об объекте обновлена');
                $location.path('/estates');
                $scope.showSpinner = false;
            }, function(){
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                $scope.showSpinner = false;
            })
        };
        $scope.initSupportModel = function(){
            $scope.selectedSupportModel = {
                name: '',
                address: '',
                phones: [
                    {
                        number: '',
                        extension: ''
                    }
                ]
            };
            $scope.selectedSupportIndex = '';
            $scope.isEditMode = false;
        };

        $scope.addSupport = function(){
            $scope.estateModel.supports_attributes.push($scope.selectedSupportModel);
            $scope.initSupportModel();
            $('#add-support-dialog').modal('hide');
        };
        $scope.updateSupport = function(){
            $scope.estateModel.supports_attributes[$scope.selectedSupportIndex] = $scope.selectedSupportModel;
            $scope.initSupportModel();
            $('#add-support-dialog').modal('hide');
        };
        $scope.setSelectedSupportIndex = function(index){
            $scope.selectedSupportIndex = index;
        };
        $scope.deleteSelectedSupport = function(index){
            $scope.estateModel.supports_attributes[index]._destroy = true;
            $('#remove-dialog').modal('hide');
        };
        $scope.setSelectedSupportData = function(index){
            $scope.isEditMode = true;
            $scope.setSelectedSupportIndex(index);
            $scope.selectedSupportModel = $scope.estateModel.supports_attributes[index];
        };
        $scope.fileReaderSupported = window.FileReader != null;
        $scope.uploadLicense = function(files){
            $scope.spinnersState.uploadSpinner = true;
            if (files != null) {
                var file = files[0];
                if ($scope.fileReaderSupported) {
                    $timeout(function() {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function(e) {
                            $timeout(function(){
                                apiCompany.uploadLicense(file, $scope.estateModel.authenticity_token, $scope.estateModel.id).then(function(){
                                    $scope.estateModel.has_files = true;
                                    $scope.spinnersState.uploadSpinner = false;
                                }, function(){
                                    $scope.spinnersState.uploadSpinner = false;
                                    alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                                });
                            });
                        }
                    });
                }
            }
        };
        $scope.removeLicense = function(){
            $scope.spinnersState.deleteSpinner = true;
            apiCompany.deleteLicense($scope.estateModel.authenticity_token, $scope.estateModel.id).then(function(){
                $scope.spinnersState.deleteSpinner = false;
                $scope.estateModel.has_files = false;
            }, function(){
                $scope.spinnersState.deleteSpinner = false;
                alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
            });
        }

    }]);
