angular.module('company').controller('companyController', ['$scope', 'apiCompany', 'userData',
    function ($scope, apiCompany, userData) {
        $scope.userCan = userData.userCan;
        $scope.userData = userData.getData();
        $scope.token = window.__token;
        $scope.changeMode = function(){
            apiCompany.mode.change($scope.token).then(function(data){
                location.reload();
            });
        }
    }]);
