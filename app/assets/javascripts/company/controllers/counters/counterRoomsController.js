angular.module('company').controller('counterRoomsController', ['$scope', 'apiCompany', 'alertService', '$location', 'objectsList', '$route', 'FileSaver', 'Blob',
    function ($scope, apiCompany, alertService, $location, objectsList, $route, FileSaver, Blob) {
        $scope.init = function() {
            $scope.estatesModel = objectsList.data;
            if($scope.estatesModel.rooms.length > 0){
                $scope.estatesSafe = angular.copy($scope.estatesModel.rooms);
            }
            $scope.report_date = new Date();
        };
        $scope.selectReport = function(report) {
          $scope.selectedReport = report;
        };
        $scope.newReport = function(report_attributes) {
          var report = {
            room_id: report_attributes[0].room_id
          }
          report.report_entries_attributes = []
          report_attributes.forEach(function(item, i, arr) {
            report.report_entries_attributes[i] = {
              counter_type: item.counter_type,
              units: item.units,
              counter_id: item.id,
              value: item.value
            }
          });
          $scope.selectedReport = report;
        };
        // not used
        $scope.download = function(rooms) {
          var estate = {
            id: $scope.estatesModel.id,
            report_date: $scope.report_date.toString()
          }
          var current_date = new Date($scope.report_date);
          var file_name = $scope.estatesModel.short_address + '-' + current_date.getMonth() + '-' + current_date.getFullYear() + '.xls';
          apiCompany.estate.download(estate).then(function(response){
            var data = new Blob([response.data], { type: 'text/plain;charset=utf-8' });
            FileSaver.saveAs(data, file_name);
          }, function(){
              alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
          })
        };
        $scope.submitForm = function(){
            $scope.selectedReport.admin = true;
            $scope.showSpinner = true;
            if ($scope.selectedReport.id) {
              apiCompany.report.update($scope.selectedReport).then(function(){
                  $('#report-dialog').modal('hide');
                  $scope.showSpinner = false;
                  alertService.updateAlert('success', 'Показания отправлены успешно');
              }, function(){
                  alertService.updateAlert('danger', 'Невозможно изменить этот очет');
              })
            }
            else {
              apiCompany.report.create($scope.selectedReport).then(function(){
                  $('#report-dialog').modal('hide');
                  $scope.showSpinner = false;
                  $route.reload();
                  alertService.updateAlert('success', 'Показания отправлены успешно');
              }, function(){
                  alertService.updateAlert('danger', 'Невозможно создать такой очет');
              })
            }
        }
    }]);

function parseDate(input) {
  return new Date(input.getFullYear(), input.getMonth(), 1); 
}

angular.module('company').filter("currentReport", function() {
  return function(items, date) {
    var choosen_date_start = parseDate(date),
      choosen_date_end = parseDate(date);
    choosen_date_end = choosen_date_end.setMonth(choosen_date_end.getMonth()+1);
    choosen_date_start = choosen_date_start.getTime();
    var arrayToReturn = [];        
    for (var i=0; i<items.length; i++){
      var report_date = Date.parse(items[i].created_at);
      if (report_date > choosen_date_start && report_date < choosen_date_end)  {
        arrayToReturn.push(items[i]);
      }
    }
    return arrayToReturn;
  };
});
