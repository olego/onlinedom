angular.module('company').controller('informationController', ['$scope', 'apiCompany', 'alertService', '$http', '$timeout', 'NgMap',
    function ($scope, apiCompany, alertService, $http, $timeout, NgMap) {
        var vm = this;

        $scope.init = function() {
            $scope.spinnersVisibilityState = {
                formSpinner: false,
                logoSpinner: false,
                innSpinner: false,
                ogrnSpinner: false
            };
            $scope.informationModel = {
                'authenticity_token' : window.__token,
                verified: false,
                name: '',
                contact_person: '',
                phones: [],
                country: '',
                region: '',
                city: '',
                address: '',
                post_code: '',
                image: '',
                info: '',
                inn: '',
                kpp: '',
                kppImage: '',
                ogrn: '',
                ogrnImage: '',
                bank_account: '',
                bik: '',
                lat: '',
                lng: '',
                has_innkpp_file: false,
                has_ogrn_file: false,
                company_name: ''
            };
            apiCompany.getCompanyInfo().then(function(result){
                var data = result.data;
                $scope.informationModel.verified = data.company.verified;
                $scope.informationModel.name = data.name;
                for (var key in data.company){
                    if(key == 'phones'){
                        if (data.company[key] != null && data.company[key].length != 0){
                            for(var i = 0; i < data.company[key].length; i++){
                                $scope.informationModel[key].push({number: data.company[key][i].number});
                            }
                        }else{
                            $scope.informationModel.phones = [
                                {
                                    number: ''
                                }
                            ];
                        }
                    }else{
                        if(key == "coordinates" && data.company[key] != null){
                            $scope.informationModel.lat = data.company[key].lat;
                            $scope.informationModel.lng = data.company[key].lng;
                        }else {
                            $scope.informationModel[key] = data.company[key];
                        }
                    }
                }
                if(data.logo){
                    $scope.informationModel.image = data.logo;
                }
                if($scope.informationModel.phones.length == 0){
                    $scope.informationModel.phones = [
                        {
                            number: ''
                        }
                    ];
                }
                $scope.informationModel.has_innkpp_file = data.has_innkpp_file;
                $scope.informationModel.has_ogrn_file = data.has_ogrn_file;
                mapInit();
            });

            var center = 
                {
                    lat: 47.2610085,
                    lng: 39.6279999
                };
            $scope.mapCenter = center;
        };
        $scope.addPhone = function(){
            $scope.informationModel.phones.push(
                {
                    number: ''
                }
            );
        };
        $scope.removePhone = function(index){
            $scope.informationModel.phones.splice(index, 1);
        };
        $scope.fileReaderSupported = window.FileReader != null;
        $scope.uploadPhoto = function(files, type, spinner){
            $scope.spinnersVisibilityState[spinner]= true;
            if (files != null) {
                var file = files[0];
                if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
                    $timeout(function() {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function(e) {
                            $timeout(function(){
                                apiCompany.uploadImage(type, file, $scope.informationModel.authenticity_token).then(function(){
                                    $scope.spinnersVisibilityState[spinner]= false;
                                    switch (type) {
                                        case 'logo':
                                            $scope.informationModel.image = e.target.result;
                                            break;
                                        case 'innkpp':
                                            $scope.informationModel.kppImage = e.target.result;
                                            $scope.informationModel.has_innkpp_file = true;
                                            break;
                                        case 'ogrn':
                                            $scope.informationModel.ogrnImage = e.target.result;
                                            $scope.informationModel.has_ogrn_file = true;
                                            break;
                                    }
                                }, function(){
                                    $scope.spinnersVisibilityState[spinner]= false;
                                    alertService.hideAlert();
                                    alertService.setText('Что-то пошло не так, попробуйте снова');
                                    alertService.setClass('alert-danger');
                                    alertService.showAlert();
                                });
                            });
                        }
                    });
                }
            }
        };
        $scope.removeLogo = function(elementId, type, spinner){
            $('#' + elementId).val('');
            $scope.spinnersVisibilityState[spinner] = true;
            apiCompany.deleteImage(type, $scope.informationModel.authenticity_token).then(function(){
                $scope.spinnersVisibilityState[spinner] = false;
                switch(type){
                    case 'logo':
                        $scope.informationModel.image = '';
                        break;
                    case 'innkpp':
                        $scope.informationModel.kppImage = '';
                        $scope.informationModel.has_innkpp_file = false;
                        break;
                    case 'ogrn':
                        $scope.informationModel.ogrnImage = '';
                        $scope.informationModel.has_ogrn_file = false;
                        break;
                }
            }, function(){
                $scope.spinnersVisibilityState[spinner] = false;
                alertService.hideAlert();
                alertService.setText('Что-то пошло не так, попробуйте снова');
                alertService.setClass('alert-danger');
                alertService.showAlert();
            });
        };
        function mapInit(){
            if($scope.informationModel.lat && $scope.informationModel.lng){

                var center = 
                    {
                        lat: Number($scope.informationModel.lat),
                        lng: Number($scope.informationModel.lng)
                    };
                setMarker(center);
            }
            $scope.mapCenter = center;
        };
        $scope.submitForm = function(){
            $scope.spinnersVisibilityState.formSpinner= true;
            apiCompany.updateCompanyInfo($scope.informationModel)
                .then(function(response){
                    $scope.spinnersVisibilityState.formSpinner = false;
                    $scope.showSpinner = false;
                    alertService.updateAlert('success', 'Обновлено успешно');
                }, function(response){
                    $scope.spinnersVisibilityState.formSpinner = false;
                    alertService.hideAlert();
                    var errorText;
                    if(response.status == 422 ) {
                        errorText = 'Текущий пользователь не принадлежит никакой компании';
                    }else{
                        if(response.status == 404){
                            errorText = 'Компания не найдена';
                        }else{
                            errorText = 'Что-то пошло не так, попробуйте повторить позднее';
                        }
                    }
                    alertService.updateAlert('danger', errorText);
                });
        }

        $scope.findAddress = function() {
          $http({
            method: 'GET',
            url: 'http://maps.google.com/maps/api/geocode/json?address=' + $scope.informationModel.city
          }).then(function successCallback(response) {
              var center = response.data.results[0].geometry.location;
              $scope.informationModel.lat = center.lat;
              $scope.informationModel.lng = center.lng;
              setMarker(center);
              $scope.mapCenter = {lat: center.lat, lng: center.lng};
              $scope.mapZoom = 10;
            }, function errorCallback(response) {
            });
        }
        function setMarker(coord) {
          if (vm.markerClusterer != undefined)
            vm.markerClusterer.clearMarkers();
          vm.dynMarkers = [];
          NgMap.getMap().then(function(map) {
            var latLng = new google.maps.LatLng(coord.lat, coord.lng)
            var marker = new google.maps.Marker({position: latLng, id: coord, draggable: true})
            marker.addListener('drag', function() {
              $scope.informationModel.lat = marker.position.lat();
              $scope.informationModel.lng = marker.position.lng();
            });
            vm.dynMarkers.push(marker);
            vm.markerClusterer = new MarkerClusterer(map, vm.dynMarkers, {});
          });

        }
    }]);
