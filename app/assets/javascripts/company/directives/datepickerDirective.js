angular.module('company').directive('timeDatePicker', [
    '$filter',
    '$sce',
    function ($filter, $sce) {
        return {
            restrict: 'AE',
            replace: false,
            scope: {
                _modelValue: '&ngModel',
                //date: "="
                date : "="
            },
            require: 'ngModel',
            templateUrl: 'template/datepicker',
            link: function (scope, element, attrs, ngModel) {
                var ref;
                scope._mode = (ref = attrs.defaultMode) != null ? ref : 'date';
                scope._displayMode = attrs.displayMode;
                ngModel.$render = function () {
                    scope.date = scope.date != null ? new Date(scope.date) : new Date();
                    scope.calendar._year = scope.date.getFullYear();
                    return scope.calendar._month = scope.date.getMonth();
                };
                scope.save = function () {
                    return scope._modelValue = scope.date;
                };
                return scope.cancel = function () {
                    return ngModel.$render();
                };
            },
            controller: [
                '$scope',
                function (scope) {
                    scope.showPicker = false;
                    if(scope.date){
                      scope.date = new Date(scope.date);
                    }else{
                      scope.date = new Date();
                    }
                    scope.display = {
                        title: function () {
                            if (scope._mode === 'date') {
                                return $filter('date')(scope.date, 'EEEE h:mm a');
                            } else {
                                return $filter('date')(scope.date, 'MMMM d yyyy');
                            }
                        },
                        'super': function () {
                            if (scope._mode === 'date') {
                                return $filter('date')(scope.date, 'MMM');
                            } else {
                                return '';
                            }
                        },
                        main: function () {
                            return $sce.trustAsHtml(scope._mode === 'date' ? $filter('date')(scope.date, 'd') : $filter('date')(scope.date, 'h:mm') + '<small>' + $filter('date')(scope.date, 'a') + '</small>');
                        },
                        sub: function () {
                            if (scope._mode === 'date') {
                                return $filter('date')(scope.date, 'yyyy');
                            } else {
                                return $filter('date')(scope.date, 'HH:mm');
                            }
                        }
                    };
                    scope.calendar = {
                        _month: 0,
                        _year: 0,
                        _months: [
                            'Январь',
                            'Февраль',
                            'Март',
                            'Апрель',
                            'Май',
                            'Июнь',
                            'Июль',
                            'Август',
                            'Сентябрь',
                            'Октябрь',
                            'Ноябрь',
                            'Декабрь'
                        ],
                        offsetMargin: function () {
                            return new Date(this._year, this._month).getDay() * 3.6 + 'rem';
                        },
                        isVisible: function (d) {
                            return new Date(this._year, this._month, d).getMonth() === this._month;
                        },
                        'class': function (d) {
                            if (new Date(this._year, this._month, d).getTime() === new Date(scope.date).setHours(0, 0, 0, 0)) {
                                return 'selected';
                            } else if (new Date(this._year, this._month, d).getTime() === new Date().setHours(0, 0, 0, 0)) {
                                return 'today';
                            } else {
                                return '';
                            }
                        },
                        select: function (d) {
                            scope.showPicker = false;
                            return scope.date.setFullYear(this._year, this._month, d);
                        },
                        monthChange: function () {
                            if (this._year == null || isNaN(this._year)) {
                                this._year = new Date().getFullYear();
                            }
                            scope.date.setFullYear(this._year, this._month);
                            if (scope.date.getMonth() !== this._month) {
                                return scope.date.setDate(0);
                            }
                        }
                    };
                    scope.currentMonth = scope.calendar._months[scope.date.getMonth()];
                    scope.setNow = function () {
                        return scope.date = new Date();
                    };
                    scope._mode = 'date';
                }
            ]
        };
    }
]);
