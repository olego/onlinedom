angular.module('company').directive('estatesStepsAddition', [ function($) {
    return {
        restrict: 'E',
        replace: true,
        scope: false,
        templateUrl: 'template/directives/estate-step-addition',
        link: function(scope, element, attrs){
            scope.$watch('estateForm.$invalid', function(newValue, oldValue) {
                if(oldValue !== newValue){
                    if(newValue === false){
                        scope.$parent.estateModel = scope.estateModel;
                        scope.$parent.buttonDisabled = false;
                    }else{
                        scope.$parent.buttonDisabled = true;
                    }
                }
            }, true);
        }
    }
}]);
