angular.module('company').directive('estatesStepsList', [ 'apiCompany', 'alertService', function(apiCompany, alertService) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            list: '='
        },
        templateUrl: 'template/directives/estate-step-list',
        link: function(scope, element, attrs){
            if (!scope.list) {
                scope.list = [];
            }

            scope.listSafe = angular.copy(scope.list.map(function(room) {
                if (!room.owner) {
                    room.owner = {
                        phones: []
                    };
                } else {
                    if (!room.owner.phones) {
                        room.owner.phones = [];
                    }
                }

                return room;
            }));
            scope.editRoom = {
                owner: {
                    phones: []
                }
            };
            scope.editRoomIndex = 0;

            scope.setRoomToEdit = function(index){
                scope.editRoom = angular.copy(scope.list[index]);
                scope.editRoomIndex = index;
            };

            scope.clearEditRoom = function(){
                scope.editRoom = {
                    owner: {
                        phones: []
                    }
                };
                scope.editRoomIndex = 0;
            };

            scope.saveRoom = function(){
                scope.editRoom.authenticity_token = window.__token;
                apiCompany.rooms.update(scope.editRoom).then(function(result){
                    scope.list[scope.editRoomIndex] = result.data;
                    alertService.updateAlert('success', 'Помещение отредактировано');
                    $('#edit-dialog').modal('hide');
                    scope.clearEditRoom();
                },function(err){
                    alertService.updateAlert('danger', 'Что-то пошло не так, попробуйте повторить позднее');
                });
            };

            scope.addPhone = function(){
                scope.editRoom.owner.phones.push('');
            };
            scope.removePhone = function(index){
                scope.editRoom.owner.phones.splice(index, 1);
            };
        }
    };
}]);
