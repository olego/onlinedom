//= require jquery
//= require jquery_ujs
//= require bootstrap/dist/js/bootstrap
//= require bootstrap-material-design/dist/js/material
//= require bootstrap-material-design/dist/js/ripples
//= require angular/angular
//= require angular-route/angular-route
//= require angular-animate/angular-animate
//= require ngmap/build/scripts/ng-map
//= require ngmap/spec/lib/markerclusterer.js
//= require angular-ui-mask/dist/mask.js
//= require material-date-picker/build/mbdatepicker.js
//= require moment/moment
//= require moment/locale/ru
//= require angular-smart-table/dist/smart-table.js
//= require angular-messages/angular-messages.js
//= require angular-ui-select/dist/select.js
//= require angular-sanitize/angular-sanitize.js
//= require moment/moment.js
//= require angular-bootstrap-datetimepicker/src/js/datetimepicker.js
//= require angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js
//= require angular-date-time-input/src/dateTimeInput.js
//= require angular-file-saver/dist/angular-file-saver.bundle.js
//= require_self

$(function() {$.material.init()});
