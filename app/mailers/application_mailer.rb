class ApplicationMailer < ActionMailer::Base
  default from: "noreply@skovalev.ru"
  layout 'mailer'

  before_action { @domain = Rails.application.config.domain }
end
