class RequestMailer < ApplicationMailer
  def start_request_email(user, request)
    @user = user
    @request_id = request.id
    @message = request.meta[:description]
    mail(
      to: @user.email,
      subject: 'Заявка принята'
    )
  end
end
