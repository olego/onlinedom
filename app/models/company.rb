class Company < ActiveRecord::Base
  attr_accessor :skip_validations, :file_errors

  has_one :user, as: :meta
  has_many :files, as: :owner, class_name: 'FileHandler'
  has_many :employees
  has_many :roles
  has_many :estates
  has_many :rooms, through: :estates
  has_many :tickets, through: :rooms

  has_many :reports, through: :estates

  has_many :tariffs, through: :estates

  serialize :phones
  serialize :coordinates

  def set_file(file, type)
    # находим старый файл
    prev_file = get_file(type)

    # если прислали новый файл-сохраняем
    if file
      fh = FileHandler.new(
          file: file,
          owner: self,
          is_public: type == 'logo',
          description: type
      )
      unless saved = fh.save
        self.file_errors = fh.errors
      end
    end

    # если новый файл сохранился-удаляем старый
    if saved && prev_file
      prev_file.destroy
    end
    return fh && fh.save
  end

  def get_file(type)
    self.files.find_by(description: type)
  end

  def logo_file
    self.files.find_by(description: 'logo')
  end

  def innkpp_file
    self.files.find_by(description: 'innkpp')
  end

  def ogrn_file
    self.files.find_by(description: 'ogrn')
  end

  def verify!
    @skip_validations = true
    return self.update(verified: true)
  end
end
