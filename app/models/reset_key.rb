class ResetKey < ActiveRecord::Base
  belongs_to :user
  validates :user_id, :key, presence: true

  def generate_key
    self.key = rand(36**64).to_s(36)
  end

  def outdated?
    return (Time.now - created_at) > 24*60*60
  end
end
