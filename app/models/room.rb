class Room < ActiveRecord::Base
  include HashWorker

  before_create :link_estate

  belongs_to :user
  belongs_to :estate

  has_many :counters, dependent: :destroy
  has_many :reports, dependent: :destroy
  has_many :tickets, dependent: :destroy

  accepts_nested_attributes_for :counters, allow_destroy: true

  serialize :meta
  serialize :map
  serialize :owner

  validates :address, presence: {
    message: 'Введите адрес квартиры'
  }
  validates :number, presence: {
    message: 'Введите номер квартиры'
  }

  enum room_type: [
    'Квартира', 
    'Нежилое помещение',
    'Дом',
    'Земельный участок',
    'Гараж',
    'Дача'
  ]

  scope :actual, -> { where(deleted: false) }

  def current_report
    self.reports
      .where(["created_at >  ?", Time.now - 1.month])
      .includes(:report_entries).last || false
  end

  def current_report_period
    last_report = self.current_report
    if last_report
      {start: last_report.created_at, end: (last_report.created_at + 1.month) }
    else
      now = Time.now
      return {
        start: Time.now - 1.month,
        end: Time.now
      }
    end
  end

  def set_owner(owner)
    if owner && owner['email']
      user = User.find_by(email: owner['email'])
      if user
        self.user_id = user.id
        user.update({
          'name' => owner['name'],
          'email' => owner['email'],
          'phones' => owner['phones'] || [''],
        })
        return self.owner = {registered: true}
      end
    end

    return set_unregistered_owner(owner)
  end

  def get_current_report str_date
    #report_date = Date.parse str_date
    #current_report = reports.where('extract(month  from created_at) = ?', report_date.month).where('extract(year  from created_at) = ?', report_date.year).first
    current_report = reports.first
    previos_report = reports.last
    data = []
    # current_report.report_entries.each do |entry|
    #   data << {
    #     counter_type: entry.counter.counter_type.to_s,
    #     current_value: entry.value.to_s,
    #     counter_num: entry.counter.seal_number
    #   }
    # end
    counters = Counter.includes(:report_entries).where(room_id: self.id)
    counters.each do |counter|
      cur_value = counter.report_entries.where(report: current_report).first.try(:value).to_s if counter.report_entries.where(report: current_report).first
      prev_value = counter.report_entries.where(report: previos_report).first.try(:value).to_s if counter.report_entries.where(report: previos_report).first 
      data << {
        counter_type: counter.counter_type.to_s,
        current_value: cur_value,
        previos_value: prev_value,
        counter_num: counter.seal_number
      }
    end
    data
  end

  private

  def set_unregistered_owner(owner)
    self.owner = {
      'name' => owner['name'],
      'email' => owner['email'],
      'phones' => owner['phones'] || [''],
      'registered' => false
    }
  end

  def link_estate
    estate = Estate.find_by(full_address: self.address)

    return true unless estate      
    if estate
      same_room = estate.rooms
        .where(
          'number = ? AND room_type = ?',
          self.number, Room.room_types[self.room_type]
        ).take

      if same_room && same_room.user_id
        self.errors[:user_id] = "Это помещение уже зарегестрировано другим пользователем"
        return false
      else
        self.estate_id = estate.id
        same_room.destroy if same_room
      end
    end
  end
  
  def self.to_csv(report_date, full_address)
    full_address.gsub!(',','')
    CSV.generate(col_sep: "\t") do |csv|
      csv << ['', full_address]
      csv << ['№', 'Владелец', 'Способ связи', 'Показания счетчиков']
      csv << ['', '', '', 'Виды услуг', 'Серийный №', 'В пред мес', 'Текущие', 'Новые']
      all.each do |room|
        arr = []
        arr << room.number
        if room.user
          arr << room.user.name
        else
          arr << '-'
        end
        if room.user
          arr << room.user.email
        else
          arr << '-'
        end
        if room.reports.count > 0
          report_data = room.get_current_report(report_date)
          arr << HashWorker.str_values(report_data, :counter_type)
          arr << HashWorker.str_values(report_data, :counter_num)
          arr << HashWorker.str_values(report_data, :previos_value)
          arr << HashWorker.str_values(report_data, :current_value)
          arr << '-'
        else
          arr << '-'
        end
        csv << arr
      end
    end
  end
end
  