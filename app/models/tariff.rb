class Tariff < ActiveRecord::Base
  enum name: [ 'Содержание общего имущества' ]

  belongs_to :estate
  has_many :archive_tariffs, dependent: :destroy

  validates :name, uniqueness: { scope: :estate_id }

  after_update :create_archive_tariff

  private

  def create_archive_tariff
    if value_changed?
      ArchiveTariff.create(tariff_id: id, old_value: value_was, new_value: value)
    end
  end
end
