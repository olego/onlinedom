class ReportEntry < ActiveRecord::Base
  attr_accessor :units
  belongs_to :report
  belongs_to :counter

  validates :counter_id, presence: true
  validates :value, presence: true, numericality: {
    :greater_than_or_equal_to => 0
  }
end