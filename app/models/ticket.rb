class Ticket < ActiveRecord::Base
  belongs_to :user
  belongs_to :executor, class_name: 'User'
  belongs_to :room
  has_many :comments, as: :commentable
  has_many :subscriptions
  has_many :subscribers, through: :subscriptions, source: :subscriber

  before_create :set_defaults
  before_save :check_finish, :send_emails, :check_subscribers

  enum status: ['Создана', 'Просмотрена', 'В работе', 'Отклонено', 'Исполнено', 'Отменена', 'Закрыта']
  enum ticket_type: ['Обычная', 'Экстренная', 'Платная']

  validates :user, presence: true
  validates :room, presence: true

  scope :finished, -> { where.not(finished_at: nil) }

  serialize :meta

  def cancel
    self.status = 'Отменена'
    return self.save
  end

  def watch!(current_user)
    watch_subscription current_user
    if self.executor_id == current_user.id && self.status == 'Создана'
      self.status = 'Просмотрена'

      return self.save
    end
    return false
  end

  def owned_by(user)
    return true if self.user.id == user.id || self.executor_id == user.id
    if self.room && self.room.estate && self.room.estate.company && user.company
      return self.room.estate.company.id = user.company.id
    end
    return false
  end

  def ticket_company
    if self.room && self.room.estate && self.room.estate.company
      return self.room.estate.company
    end
  end

  def editable(user)
    return false if ['Отменена', 'Закрыта', 'Отклонено'].include?(self.status)

    if user.user_type == 'company'
      return user.company == self.ticket_company
    else 
      return owned_by(user)
    end
  end

  def set_subscribers
    subscribers = []
    subscribers << self.user
    subscribers << self.executor if self.executor
    if self.room && self.room.estate && self.room.estate.company
      company = self.room.estate.company
      subscribers << company.user
      company.roles.each do |role|
        if role.permissions['requests'] && role.permissions['requests']['list']
          role.employees.each do |employee|
            subscribers << employee.user unless subscribers.include? employee
          end
        end
      end
    end
    self.subscribers = subscribers.uniq
  end

  def update_subscriptions(except_user)
    self.subscriptions.where.not(user_id: except_user.id).each do |subscription|
      subscription.unread!
    end
  end

  def watch_subscription(user)
    subscription = self.subscriptions.find_by(user_id: user.id)
    subscription.read! if subscription
  end

  def get_subscriptions user
    subscription = subscriptions.find_by(user_id: user.id)
    subscription = Subscription.create(user_id: user.id, ticket_id: self.id) unless subscription
    subscription
  end

  private
  def set_defaults
    self.status ||= 'Создана'
    self.ticket_type ||= 'Обычная' 

    if self.room.estate && self.room.estate.company
      self.number = self.room.estate.company.tickets.count + 1
    else
      self.number = nil
    end
  end

  def check_finish
    if self.changed? && self.changes[:status]
      self.finished_at = self.status == "Исполнено" ? Time.now : nil
    end
  end

  def send_emails
    if self.changed? && self.changes[:executor_id] && !self.changes[:executor_id][0]
      RequestMailer.start_request_email(self.user, self).deliver_now
      RequestMailer.start_request_email(self.executor, self).deliver_now
    end
  end

  def check_subscribers
    if self.changed? && self.changes[:executor_id]
      set_subscribers
    end
  end
end
