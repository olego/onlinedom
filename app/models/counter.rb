class Counter < ActiveRecord::Base
    CUBE_METERS = 'm3'
    KWT_PER_HOUR = "kwth"
    GIGAKAL = "gkal"
    @@units = {
        'Газ' => CUBE_METERS,
        'Х.водоснабжение' => CUBE_METERS,
        'Г.водоснабжение' => CUBE_METERS,
        'Электричество' => KWT_PER_HOUR,
        'Отопление' => GIGAKAL,
        'Водоотведение' => CUBE_METERS
    }

    def self.units
        @@units
    end

    def units
        @@units[self.counter_type] || 'ед.'
    end

    belongs_to :room
    enum place: [
        'Подъезд',
        'Ванная',
        'Туалет',
        'Кухня',
        'Другое'
    ]
    enum counter_type: [
        'Газ', 'Х.водоснабжение', 'Г.водоснабжение', 'Электричество',
        'Отопление', 'Водоотведение'
    ]
    has_many :report_entries
end
