class Estate < ActiveRecord::Base
  @@address_fields = {
    'region' => 'Область',
    'city' => 'Населенный пункт',
    'street' => 'Улица',
    'number' => 'Номер дома',
    'building' => 'Строение',
    'housing' => 'Корпус'
  }
  @@required_address_fields = ['region', 'city', 'street', 'number']

  belongs_to :company
  has_many :supports, dependent: :destroy
  has_many :rooms, dependent: :destroy

  has_many :reports, through: :rooms

  has_many :tariffs, dependent: :destroy

  enum estate_type: ['apartaments_house']

  serialize :meta
  serialize :license
  serialize :address

  before_validation :assemble_address

  validates :full_address, presence: { message: 'Необходимо указать адрес' },
                           uniqueness: { message: 'Дом с таким адресом уже есть в системе' }

  after_create :create_rooms

  def self.address_fields
    @@address_fields
  end

  def short_address
    self.address['building'] + '-' + self.address['street']
  end

  private

  def address_has_errors?
    errors[:address].clear
    has_errors = false

    unless address
      errors.add(:address, "Не указан адрес")
      return true
    end

    @@required_address_fields.each do |field|
      if !self.address[field] || address[field].to_s.empty?
        errors.add(:address, "Не заполнено поле '#{@@address_fields[field]}'")
        has_errors = true
      end
    end
    has_errors
  end

  def assemble_address
    self.full_address = ''
    return if address_has_errors?

    self.full_address += self.address['region'].to_s + ' область, '
    self.full_address += self.address['city'].to_s + ', '
    self.full_address += 'ул. ' + self.address['street'].to_s + ' '
    self.full_address += 'д. ' + self.address['number'].to_s + ' '

    if self.address['building'] && !self.address['building'].to_s.strip.empty?
      self.full_address += 'строение ' + self.address['building'].to_s + ' '
    end

    if self.address['housing'] && !self.address['housing'].to_s.strip.empty?
      self.full_address += 'корп. ' + self.address['housing'].to_s + ' '
    end

    self.full_address.strip!
  end

  def create_rooms
    @existing_rooms = Room.all.where(address: self.full_address)
    @existing_rooms.each {|room| room.update(estate: self)}

    return unless self.meta
    Room.transaction do
      housing_number = self.meta['housing_number'] && self.meta['housing_number'].to_i
      if housing_number
        create_bunch_rooms(housing_number, 'Квартира');
      end

      nonresident_number = self.meta['nonresident_number'] && self.meta['nonresident_number'].to_i
      if nonresident_number 
        create_bunch_rooms(nonresident_number, 'Нежилое помещение');
      end
    end
  end

  def create_bunch_rooms(quantity, type)
    (1..quantity).each do |number|
      next if @existing_rooms
              .where("number = ? AND room_type = ?", number, Room.room_types[type])
              .count > 0

      Room.create(
        estate: self,
        room_type: type,
        address: self.full_address,
        number: number
      )
    end
  end

end
