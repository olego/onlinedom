class Subscription < ActiveRecord::Base
  belongs_to :subscriber, class_name: 'User',
                          foreign_key: "user_id"
  belongs_to :ticket

  def unread!
    self.update(watched: false)
  end

  def read!
    self.update(watched: true)
  end
end
