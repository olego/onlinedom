User.create([
  {
    name: 'Admin', email: 'admin@onlinedom.info',
    password: 'jm50tyn2', password_confirmation: 'jm50tyn2',
    user_type: 'admin', confirmed: true
  },
  {
    name: 'Stanislav Kovalev', email: 'djently@yandex.ru',
    password: 'jm50tyn2', password_confirmation: 'jm50tyn2',
    user_type: 'client', confirmed: true,
    phones: [{number: '74912904639'}, {number: '79209519525'}]
  },
  {
    name: 'Vasili Pupkin', email: 'vpupkin@onlinedom.info',
    password: 'jm50tyn2', password_confirmation: 'jm50tyn2',
    user_type: 'client'
  }
])

company_user = User.create(
  name: 'DomGon', email: 'user@domgon.ru',
  password: 'jm50tyn2', password_confirmation: 'jm50tyn2',
  user_type: 'company', confirmed: true
)

# User.create(
#   name: 'Artur', email: 'vlad2@tut.by',
#   password: '123321', password_confirmation: '123321',
#   confirmed: true, user_type: 'client'
# )

estates = Estate.create([
{  
  address: {
    'region' => '___Рязанская Область',
    'city' => '___Рязань',
    'street' => '___Братиславская',
    'number' => '___21'
  },
  meta: {
    housing_number: 1,
    nonresident_number: 1
  },
  company: company_user.company
},
{  
  address: {
    'region' => '___Московская Область',
    'city' => '___Москва',
    'street' => '___Ленинские горы',
    'number' => '___1'
  },
  company: company_user.company
}
])
