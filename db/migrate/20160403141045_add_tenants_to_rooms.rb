class AddTenantsToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :tenants, :integer
  end
end
