class AddDescriptionToFileHandlers < ActiveRecord::Migration
  def change
    add_column :file_handlers, :description, :string
  end
end
