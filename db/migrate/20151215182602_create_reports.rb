class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.references :room

      t.timestamps null: false
    end
  end
end
