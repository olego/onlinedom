class CreateFileHandlers < ActiveRecord::Migration
  def change
    create_table :file_handlers do |t|
      t.string :name
      t.boolean :is_public
      t.integer :owner_id
      t.string :owner_type

      t.timestamps null: false
    end

    add_index :file_handlers, :owner_id
  end
end
