class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :company_name
      t.string :contact_person
      t.text :phones
      t.string :country
      t.string :region
      t.string :city
      t.string :address
      t.string :post_code
      t.string :inn
      t.string :kpp
      t.string :ogrn
      t.string :bank_account
      t.string :bik
      t.string :coordinates
      t.boolean :verified
      t.text :info

      t.timestamps null: false
    end
  end
end
