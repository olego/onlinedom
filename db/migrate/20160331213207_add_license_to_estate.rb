class AddLicenseToEstate < ActiveRecord::Migration
  def change
    remove_column :estates, :map
    add_column :estates, :license, :text
  end
end
