class CreateArchiveTariffs < ActiveRecord::Migration
  def change
    create_table :archive_tariffs do |t|
      t.references :tariff, index: true, foreign_key: true
      t.decimal :old_value
      t.decimal :new_value

      t.timestamps null: false
    end
  end
end
