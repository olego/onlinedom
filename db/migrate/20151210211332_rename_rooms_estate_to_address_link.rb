class RenameRoomsEstateToAddressLink < ActiveRecord::Migration
  def change
    remove_column :rooms, :estate_id
    add_column :rooms, :address_link_id, :integer, index: true, foreign_key: true
  end
end
