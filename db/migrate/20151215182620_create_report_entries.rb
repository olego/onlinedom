class CreateReportEntries < ActiveRecord::Migration
  def change
    create_table :report_entries do |t|
      t.references :report
      t.references :counter
      t.float :value
    end
  end
end
