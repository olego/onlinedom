class CreateTariffs < ActiveRecord::Migration
  def change
    create_table :tariffs do |t|
      t.integer :name
      t.decimal :value
      t.references :estate, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
