class ChangeEstateRoomsAssociation < ActiveRecord::Migration
  def change
    drop_table :address_links
    add_column :rooms, :estate_id, :integer, index: true
  end
end
