class RenameRoomsDescriptionToMeta < ActiveRecord::Migration
  def change
    rename_column :rooms, :description, :meta
  end
end
