class AddWatchedToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :watched, :boolean, default: false
  end
end
