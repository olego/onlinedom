class AddBirthdateAndGenderToUsers < ActiveRecord::Migration
  def change
    add_column :users, :birthdate, :date
    add_column :users, :gender, :integer
    add_column :users, :phones, :text
  end
end
