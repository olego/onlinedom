class AddDeletedToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :deleted, :boolean, default: false
  end
end
