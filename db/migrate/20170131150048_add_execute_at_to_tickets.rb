class AddExecuteAtToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :execute_at, :datetime
  end
end
