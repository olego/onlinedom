class CreateEstates < ActiveRecord::Migration
  def change
    create_table :estates do |t|
      t.string :address, index: true
      t.integer :estate_type
      t.text :meta
      t.references :company

      t.timestamps null: false
    end
  end
end
