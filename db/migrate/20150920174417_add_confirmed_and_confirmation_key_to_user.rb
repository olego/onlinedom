class AddConfirmedAndConfirmationKeyToUser < ActiveRecord::Migration
  def change
    add_column :users, :confirmed, :boolean, default: false
    add_column :users, :confirmation_key, :string
  end
end
