namespace :db do
  task close_requests: :environment do
    Ticket.finished.each do |request|
      if (Time.now - request.finished_at.time)/3600/24 >= 1
        request.finished_at = nil
        request.status = "Закрыта"
        request.save
      end
    end
  end
end
