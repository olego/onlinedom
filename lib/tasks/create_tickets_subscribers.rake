namespace :db do
  task create_tickets_subscribers: :environment do
    Ticket.all.each do |request|
      request.set_subscribers
    end
  end
end
