lock '3.7.2'

set :application, 'OnlineDom'
set :repo_url, 'git@gitlab.monostacklabs.com:monostack-backend/onlinedom.git'

set :deploy_to, '/home/deploy/'
set :branch, 'master'
set :keep_releases, 3

set :format, :pretty
set :log_level, :debug
set :pty, true

set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads}
set :linked_files, %w{config/database.yml config/secrets.yml}
set :bundle_binstubs, nil

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end
