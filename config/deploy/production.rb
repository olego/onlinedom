set :stage, :production
set :app_environment, "production"

set :ssh_options, { forward_agent: true, port: 22 }
set :branch, 'master'
set :deploy_to, '/home/deploy'

server '88.212.253.175', user: 'deploy', roles: %w{web app db}
